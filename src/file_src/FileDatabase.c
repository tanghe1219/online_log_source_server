#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <dirent.h>
#include <pthread.h>
#include <ctype.h>
#include <fcntl.h>
#include <sys/types.h>
#include <unistd.h>
#include <errno.h>
#include <sys/stat.h>
#include<unistd.h>
#include "../file_include/FileDatabase.h"
#include "../file_include/apollo_list.h"
#include "../file_include/apollo_errno.h"
#include "../file_include/apollo_safeop.h"
#include "../file_include/apollo_systype.h"

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <errno.h>
#include <sys/stat.h>
#include <dirent.h>

#include <fcntl.h>
#include <sys/types.h>


INT8_T* mysql = "/var/log/mysql";
INT8_T* nginx = "var/log/nginx";
INT8_T* appdir = "/var/log";
#define MYSQL "mysql"
#define NGINX "nginx"
#define TOMCAT "tomcat"
#define APACHE "apache"
#define MONGO "mongo"
#define REDIS "redis"
#define NORMALAPP "1"
#define UNNORMALAPP "0"

typedef enum
{
	FILE_INFO_TYPE_UNKNOWN,
	FILE_INFO_TYPE_DIR,
	FILE_INFO_TYPE_REG,
	FILE_INFO_TYPE_LNK,
	FILE_INFO_TYPE_OTHER,
}FILE_INFO_TYPE_E;

static FILE_INFO_TYPE_E  apollo_file_api_get_type(INT8_T* filepath)
{
	struct stat status;
	FILE_INFO_TYPE_E type = FILE_INFO_TYPE_UNKNOWN;

	if (stat(filepath, &status) < 0) {
		return FILE_INFO_TYPE_UNKNOWN;
	}

	if (S_ISLNK(status.st_mode)) {
		type = FILE_INFO_TYPE_LNK;
	}
	else if (S_ISDIR(status.st_mode)) {
		type = FILE_INFO_TYPE_DIR;
	}
	else if (S_ISREG(status.st_mode)) {
		type = FILE_INFO_TYPE_REG;
	}
	else {
		type = FILE_INFO_TYPE_OTHER;
	}
	return type;
}


static int apollo_file_api_file_is_dir(INT8_T* filepath)
{
	FILE_INFO_TYPE_E type = apollo_file_api_get_type(filepath);
	if (type == FILE_INFO_TYPE_DIR)
		return APOLLO_OK;
	else
		return APOLLO_ERRNO_FILE_MATCHED;
}


static int apollo_file_api_file_is_reg(INT8_T* filepath)
{
	FILE_INFO_TYPE_E type = apollo_file_api_get_type(filepath);
	if (type == FILE_INFO_TYPE_REG)
		return APOLLO_OK;
	else
		return APOLLO_ERRNO_FILE_MATCHED;
}

static INT64_T apollo_file_api_get_size(INT8_T* filepath)
{
	if (NULL == filepath) {
		return -APOLLO_ERRNO_INVALID_ARGUMENTS;
	}
	struct stat status;
	if (stat(filepath, &status) < 0) {
		return -APOLLO_ERRNO_FILE_STAT;
	}

	INT64_T filesize = status.st_size;
	return filesize;
}

static char* apollo_filepath_api_get_filename(char* filepath)
{
	if (NULL == filepath)
		return NULL;

	char* result = NULL;
	result = strrchr(filepath, '/');
	//printf("filename==%s\n",(result+1));
	return (result + 1);
}


static void scanAppLogDir(INT8_T* topdir, INT8_T* appname, FILERESPONSE_INFO_LIST** logInfos)
{
	DIR* dir;
	if (NULL == (dir = opendir(topdir))) {
		return;
	}

	INT8_T* filepath = NULL;
	struct dirent* ptr;
	while ((ptr = readdir(dir))) {
		if (ptr->d_name[0] == '.') continue;
		if (NULL == (filepath = calloc(strlen(topdir) + strlen(ptr->d_name) + 2, 1))) continue;
		sprintf(filepath, "%s/%s", topdir, ptr->d_name);
		//printf("filepath==%s\n",filepath);
		if (safe_strstr(filepath, appname) != NULL)
		{
			if (apollo_file_api_file_is_dir(filepath) == APOLLO_OK) {
				/******************************************************
				 * 目录: 递归调用scanAppLogDir
				 ******************************************************/
				scanAppLogDir(filepath, appname, logInfos);
			}
			else if (apollo_file_api_file_is_reg(filepath) == APOLLO_OK) {
				/*************************************
				*文件: 添加到应用日志信息结构体
				*************************************/
				//创建应用日志信息结构体给日志路径赋值
				FILERESPONSE_INFO* info = NULL;
				if ((info = malloc(sizeof(FILERESPONSE_INFO))))
				{
					//初始化日志信息结构
					info->filepath = strdup(filepath);
					info->filename = NULL;
					info->classname = NULL;
					info->data = NULL;
					info->type = strdup("1");
					info->fileSegStart = 0;
					info->fileSegEnd = 0;
					info->currentFileSize = 0;
					info->beforeFileSize = 0;
				}
				//插入应用日志结构体链表
				FILERESPONSE_INFO_LIST* node = NULL;
				if (NULL == (node = (FILERESPONSE_INFO_LIST*)apollo_create_list_node(info))) {
					apollo_delete_node((PVOID*)&info, NULL);
				}
				else {
					apollo_add_node_to_list((APOLLO_LIST_HEAD**)logInfos, (APOLLO_LIST_HEAD*)node, NULL, NULL, NULL);
				}
			}
		}
		safe_free(filepath);
	}
	closedir(dir);
}


#define FILEBUFFER_LENGTH 1024*1024
#define EMPTY_STR ""

//打开fileName指定的文件，从中读取第lineNumber行
//返回值：成功返回1，失败返回0
int get_file_line(char* result, char* fileName, int lineNumber)
{
	FILE* filePointer;
	int i = 0;
	int number = 0;
	char buffer[FILEBUFFER_LENGTH];
	char* temp;

	memset(buffer, '\0', FILEBUFFER_LENGTH * sizeof(char));
	strcpy(buffer, EMPTY_STR);

	if ((fileName == NULL) || (result == NULL))
	{
		return 0;
	}

	if (!(filePointer = fopen(fileName, "rb")))
	{
		return 0;
	}

	// 设置无缓冲
	setvbuf(filePointer, NULL, _IONBF, 0);

	while ((!feof(filePointer)) && (i < lineNumber))
	{
		if (!fgets(buffer, FILEBUFFER_LENGTH, filePointer))
		{
			return 0;
		}
		i++;//差点又忘记加这一句了
	}

	if (0 != fclose(filePointer))
	{
		return 0;
	}
	if (0 != strcmp(buffer, EMPTY_STR))
	{
		while (NULL != (temp = strstr(buffer, "\n")))
		{
			*temp = '\0';
		}

		while (NULL != (temp = strstr(buffer, "\r")))
		{
			*temp = '\0';
		}
		strcpy(result, buffer);
		number = strlen(buffer);
		*(result + number) = '\n';
	}
	else
	{
		*result = '\n';
		return 1;
	}
	return number + 1;
}

//根据前端参数获取文件区域内容
static void getFileRangeContents(char* filepath, char* contents, char* seg_start, char* seg_end)
{
	int i = 0;
	int count = 0;
	int lineNumber = atoi(seg_end) - atoi(seg_start);
	if (atoi(seg_start) == 0)
		i = 1;
	for (i; i <= lineNumber; i++)
	{
		if ((count = get_file_line(contents, filepath, i)))
		{
			contents = contents + count;
		}
	}
}

static void getFilepathContents(char* filepath, FILERESPONSE_INFO_LIST** logInfos)
{
	if (NULL == filepath)
		return;

	if (apollo_file_api_file_is_reg(filepath) == APOLLO_OK) {
		/*************************************
		*文件: 添加到应用日志信息结构体
		*************************************/
		//创建应用日志信息结构体给日志路径赋值
		FILERESPONSE_INFO* info = NULL;
		if ((info = malloc(sizeof(FILERESPONSE_INFO))))
		{
			//初始化日志信息结构
			info->filepath = strdup(filepath);
			info->filename = NULL;
			info->classname = NULL;
			info->apptype = NULL;
			info->data = NULL;
			info->type = strdup("1");
			info->fileSegStart = 0;
			info->fileSegEnd = 0;
			info->currentFileSize = 0;
			info->beforeFileSize = 0;
		}
		//插入应用日志结构体链表
		FILERESPONSE_INFO_LIST* node = NULL;
		if (NULL == (node = (FILERESPONSE_INFO_LIST*)apollo_create_list_node(info))) {
			apollo_delete_node((PVOID*)&info, NULL);
		}
		else {
			apollo_add_node_to_list((APOLLO_LIST_HEAD**)logInfos, (APOLLO_LIST_HEAD*)node, NULL, NULL, NULL);
		}
	}
}

int getAppType(char* appname)
{
	int ret = -1;

	if (NULL == appname)
		return ret;

	if ((strcmp(appname, MYSQL) == 0) || (strcmp(appname, NGINX) == 0) || (strcmp(appname, TOMCAT) == 0) || (strcmp(appname, APACHE) == 0) || (strcmp(appname, MONGO) == 0) || (strcmp(appname, REDIS) == 0))
		ret = 1;
	else
		ret = 0;
	return ret;
}

static char* getAppName(char* filepath)
{
	if (NULL == filepath)
		return NULL;

	if (safe_strstr(filepath, MYSQL) != NULL)
	{
		return MYSQL;
	}
	else if (safe_strstr(filepath, NGINX) != NULL)
	{
		return NGINX;
	}
	else if (safe_strstr(filepath, TOMCAT) != NULL)
	{
		return TOMCAT;
	}
	else if (safe_strstr(filepath, APACHE) != NULL)
	{
		return APACHE;
	}
	else if (safe_strstr(filepath, MONGO) != NULL)
	{
		return MONGO;
	}
	else if (safe_strstr(filepath, REDIS) != NULL)
	{
		return REDIS;
	}
	else
	{
		return NULL;
	}

}


/* 查询应用日志信息 */
void scanAppLogFilepath(char* filepath, char* appname, char* seg_start, char* seg_end, FILERESPONSE_INFO_LIST** logInfos)
{
	/******************************
	* 遍历var/log目录获取应用目录
	* 遍历应用目录下所有日志文件
	******************************/
	FILERESPONSE_INFO_LIST* infos = NULL;
	if (filepath != NULL)
	{
		//printf("11111\n");
	//通过应用日志绝对路径获取日志内容
		getFilepathContents(filepath, &infos);
	}
	else
	{
		//printf("222222\n");
		//前端参数是应用名则遍历日志目录
		scanAppLogDir(appdir, appname, &infos);
	}
	if (infos) {
		FILERESPONSE_INFO_LIST* hostlist = NULL;
		APOLLO_LIST_ENTRY(hostlist, infos) {
			if (hostlist->info) {
				//printf("filepath==%s\n",hostlist->info->filepath);
				char* filename = NULL;
				char* apptype = NULL;
				INT64_T fileSize = 0;

				fileSize = apollo_file_api_get_size(hostlist->info->filepath);
				hostlist->info->currentFileSize = fileSize;
				if ((filename = apollo_filepath_api_get_filename(hostlist->info->filepath)) != NULL)
				{
					hostlist->info->filename = strdup(filename);
				}
				char lineStr[FILEBUFFER_LENGTH];
				memset(lineStr, 0, FILEBUFFER_LENGTH);
				getFileRangeContents(hostlist->info->filepath, lineStr, seg_start, seg_end);
				//printf("Contents[\n%s]\n", lineStr);	
				hostlist->info->appname = strdup(appname);
				if (strlen(lineStr))
				{
					hostlist->info->data = strdup(lineStr);
				}
				else
				{
					hostlist->info->data = strdup("");
				}
				if (getAppType(appname) == 1)
					hostlist->info->apptype = strdup("1");
				else
					hostlist->info->apptype = strdup("0");
				hostlist->info->fileSegStart = strdup(seg_start);
				hostlist->info->fileSegEnd = strdup(seg_end);
			}
		}
	}

	*logInfos = infos;
}


//扫描当前系统常用应用日志 name、filepath
void scanAppnameOnSystem(char* topdir, FILERESPONSE_INFO_LIST** logInfos)
{
	INT8_T* filepath = NULL;
	DIR* dir;
	if (NULL == (dir = opendir(topdir))) {
		return;
	}

	struct dirent* ptr;
	while ((ptr = readdir(dir))) {
		//printf("topdir===%s\n",topdir);
		if (ptr->d_name[0] == '.') continue;
		if (NULL == (filepath = calloc(strlen(topdir) + strlen(ptr->d_name) + 2, 1))) continue;
		sprintf(filepath, "%s/%s", topdir, ptr->d_name);
		if ((safe_strstr(filepath, MYSQL) != NULL) || (safe_strstr(filepath, NGINX) != NULL) || (safe_strstr(filepath, TOMCAT) != NULL) || (safe_strstr(filepath, APACHE) != NULL) || (safe_strstr(filepath, MONGO) != NULL) || (safe_strstr(filepath, REDIS) != NULL))
		{
			if (apollo_file_api_file_is_dir(filepath) == APOLLO_OK) {
				/******************************************************
				 * 目录: 递归调用scanAppLogDir
				 ******************************************************/
				scanAppnameOnSystem(filepath, logInfos);
			}
			else if (apollo_file_api_file_is_reg(filepath) == APOLLO_OK) {
				/*************************************
				*文件: 添加到应用日志信息结构体
				*************************************/
				//创建应用日志信息结构体给日志路径赋值
				FILERESPONSE_INFO* info = NULL;
				if ((info = malloc(sizeof(FILERESPONSE_INFO))))
				{
					//初始化日志信息结构
					info->filepath = strdup(filepath);//应用日志绝对路径
					info->filename = strdup(getAppName(filepath));//应用日志名称
					info->classname = NULL;
					info->data = NULL;
					info->type = strdup(NORMALAPP);//属于常用应用日志
				}
				//插入应用日志结构体链表
				FILERESPONSE_INFO_LIST* node = NULL;
				if (NULL == (node = (FILERESPONSE_INFO_LIST*)apollo_create_list_node(info))) {
					apollo_delete_node((PVOID*)&info, NULL);
				}
				else {
					//printf("filepath==%s\n",filepath);
					apollo_add_node_to_list((APOLLO_LIST_HEAD**)logInfos, (APOLLO_LIST_HEAD*)node, NULL, NULL, NULL);
				}
			}
		}
		safe_free(filepath);
	}
	closedir(dir);
}


//查询当前系统常用应用日志
int addAppInfoToList(char* appname, char* filepath, FILERESPONSE_INFO_LIST** logInfos)
{
	int ret = 0;
	if ((NULL == appname) || (NULL == filepath))
		return ret;

	//创建应用日志信息结构体给日志路径赋值
	FILERESPONSE_INFO* info = NULL;
	if ((info = malloc(sizeof(FILERESPONSE_INFO))))
	{
		//初始化日志信息结构
		info->filepath = strdup(filepath);//应用日志绝对路径
		info->filename = strdup(appname);//应用日志名称
		info->classname = NULL;
		info->data = NULL;
		if (getAppType(appname) == 1)
			info->type = strdup(NORMALAPP);//常用应用日志
		else
			info->type = strdup(UNNORMALAPP);//自定义应用
	}
	//插入应用日志结构体链表
	FILERESPONSE_INFO_LIST* node = NULL;
	if (NULL == (node = (FILERESPONSE_INFO_LIST*)apollo_create_list_node(info))) {
		apollo_delete_node((PVOID*)&info, NULL);
	}
	else {
		ret = 1;
		apollo_add_node_to_list((APOLLO_LIST_HEAD**)logInfos, (APOLLO_LIST_HEAD*)node, NULL, NULL, NULL);
	}
	return ret;
}

VOID delete_apollo_rtmlmng_partition_information(PVOID info)
{
	FILERESPONSE_INFO* node = info;

	safe_free(node->filepath);
	safe_free(node->filename);
	safe_free(node->type);
}



//从应用日志信息链表中删除节点
int  deleteAppInfoFromList(char* filename, char* filepath, FILERESPONSE_INFO_LIST** logInfos)
{
	int ret = 0;
	FILERESPONSE_INFO_LIST* pt_listnode = *logInfos;
	FILERESPONSE_INFO_LIST* pt_prevnode = NULL;

	while (pt_listnode) {
		FILERESPONSE_INFO_LIST* pt_nextnode = pt_listnode->next;
		if (strcmp(pt_listnode->info->filename, filename) == 0) {
			if (pt_prevnode) {
				pt_prevnode->next = pt_nextnode;
			}
			apollo_delete_list_node((APOLLO_LIST_HEAD**)&pt_listnode, delete_apollo_rtmlmng_partition_information);
			ret = 1;
		}
		if (pt_listnode) pt_prevnode = pt_listnode;
		pt_listnode = pt_nextnode;
	}
	return ret;
}

//
int updateAppInfoFromList(char* filename, char* filepath, FILERESPONSE_INFO_LIST* logInfos)
{
	int ret = 0;
	FILERESPONSE_INFO_LIST* hostlist = NULL;
	APOLLO_LIST_ENTRY(hostlist, logInfos) {
		if (hostlist->info) {
			if (strcmp(hostlist->info->filename, filename) == 0)
			{
				hostlist->info->filepath = strdup(filepath);
				ret = 1;
			}
		}
	}
	return ret;
}
#if 0
void queryAppInfos(FILERESPONSE_INFO_LIST* infos)
{
	FILERESPONSE_INFO_LIST* hostlist = NULL;
	APOLLO_LIST_ENTRY(hostlist, infos) {
		if (hostlist->info) {
			//printf("appname==%s\n",hostlist->info->filename);
			//printf("filepath==%s\n",hostlist->info->filepath);
				   // printf("type==%s\n",hostlist->info->type);
		}
	}
}
#endif
void cleanupResource(FILERESPONSE_INFO_LIST* infos)
{
	FILERESPONSE_INFO_LIST* hostlist = infos, *tmp = NULL;

	while (hostlist) {
		if (hostlist->info) {
			safe_free(hostlist->info->filepath);
			safe_free(hostlist->info->filename);
			safe_free(hostlist->info->classname);
			safe_free(hostlist->info->data);
			safe_free(hostlist->info->type);
			safe_free(hostlist->info->appname);
			safe_free(hostlist->info->apptype);
			safe_free(hostlist->info->fileSegStart);
			safe_free(hostlist->info->fileSegEnd);
			safe_free(hostlist->info);
			hostlist->info = NULL;
		}

		tmp = hostlist;
		hostlist = hostlist->next;
		safe_free(tmp);
	}
}

