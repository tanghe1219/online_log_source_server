#include <string.h>
#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <iostream> 
#include <map>

using namespace std;


#include "../file_include/JsonDatabase.h"
#include "json/json.h"


/*********************************************
** 解析前端数据
*********************************************/
JSONREQUEST_INFO* parseRequstData(const char* body)
{

	//printf("body==%s\n",body);
	JSONREQUEST_INFO* request_info = NULL;
	Json::CharReaderBuilder b;
	Json::CharReader* reader(b.newCharReader());
	Json::Value root;
	JSONCPP_STRING errs;
	bool ok = reader->parse(body, body + std::strlen(body), &root, &errs);
	if (ok && errs.size() == 0)
	{
		if ((request_info = static_cast<JSONREQUEST_INFO*>(malloc(sizeof(JSONREQUEST_INFO)))))
		{
			if ((root["file_path"].asString()).c_str())
				request_info->filepath = strdup((root["file_path"].asString()).c_str());
			else
				request_info->filepath = NULL;
			if ((root["file_name"].asString()).c_str())
				request_info->appname = strdup((root["file_name"].asString()).c_str());
			else
				request_info->appname = NULL;
			request_info->type = strdup("1");
			request_info->topdir = strdup("/var/log");
			if ((root["file_seg_start"].asString()).c_str())
				request_info->seg_start = strdup((root["file_seg_start"].asString()).c_str());
			else
				request_info->seg_start = strdup("0");
			if ((root["file_seg_end"].asString()).c_str())
				request_info->seg_end = strdup((root["file_seg_end"].asString()).c_str());
			else
				request_info->seg_end = strdup("50");
		}
	}
	delete reader;

	return request_info;
}


/*********************************************
** 封装前端返回数据
*********************************************/
char* packageResponseData(FILERESPONSE_INFO_LIST* infos)
{
	if (NULL == infos)
		return NULL;
	char* response = NULL;
	FILERESPONSE_INFO_LIST* hostlist = NULL;
	Json::Value loginfo;

	for (hostlist = infos; hostlist; hostlist = static_cast<FILERESPONSE_INFO_LIST*>(hostlist->next))
	{
		if (infos->info)
		{
			loginfo["type"] = "1";
			loginfo["file_name"] = hostlist->info->appname;
			loginfo["file_path"] = hostlist->info->filepath;
			loginfo["class"] = "1";
			loginfo["app_type"] = hostlist->info->apptype;
			loginfo["file_seg_start"] = hostlist->info->fileSegStart;
			loginfo["file_seg_end"] = hostlist->info->fileSegEnd;
			loginfo["data"] = hostlist->info->data;
		}
	}
	//array["array"] = Json::Value(root);
	Json::StreamWriterBuilder jswBuilder;
	jswBuilder["emitUTF8"] = true;
	std::unique_ptr<Json::StreamWriter>jsWriter(jswBuilder.newStreamWriter());
	std::ostringstream os;
	jsWriter->write(loginfo, &os);
	response = strdup(os.str().c_str());
	return response;
}

char* queryAppInfos(FILERESPONSE_INFO_LIST* infos)
{
	if (NULL == infos)
		return NULL;
	char* response = NULL;
	FILERESPONSE_INFO_LIST* hostlist = NULL;
	Json::Value array;
	Json::Value root;

	for (hostlist = infos; hostlist; hostlist = static_cast<FILERESPONSE_INFO_LIST*>(hostlist->next))
	{
		if (hostlist->info)
		{
			//printf("1111hostlist->info->appname==%s\n",hostlist->info->filename);
			Json::Value loginfo;
			if (getAppType(hostlist->info->filename) == 1)
				loginfo["is_default"] = true;
			else
				loginfo["is_default"] = false;
			loginfo["file_name"] = hostlist->info->filename;
			loginfo["file_path"] = hostlist->info->filepath;
			array.append(loginfo);
			//printf("2222hostlist->info->filename==%s\n",hostlist->info->filename);
		}
	}
	root["data"] = Json::Value(array);
	Json::StreamWriterBuilder jswBuilder;
	jswBuilder["emitUTF8"] = true;
	std::unique_ptr<Json::StreamWriter>jsWriter(jswBuilder.newStreamWriter());
	std::ostringstream os;
	jsWriter->write(root, &os);
	response = strdup(os.str().c_str());
	//printf("query app infos response==%s\n",response);
	return response;
}

char* responseCodeMsg(int code, const char* msg)
{
	if (NULL == msg)
		return NULL;
	char* response = NULL;
	Json::Value msgInfo;
	Json::StreamWriterBuilder jswBuilder;

	msgInfo["code"] = code;
	msgInfo["msg"] = msg;

	jswBuilder["emitUTF8"] = true;
	std::unique_ptr<Json::StreamWriter>jsWriter(jswBuilder.newStreamWriter());
	std::ostringstream os;
	jsWriter->write(msgInfo, &os);
	response = strdup(os.str().c_str());

	return response;
}

char* responseErrorMsg(const char* type, const char* msg)
{
	if ((NULL == type) || (NULL == msg))
		return NULL;
	char* response = NULL;
	Json::Value msgInfo;
	Json::StreamWriterBuilder jswBuilder;

	msgInfo["error"] = type;
	msgInfo["msg"] = msg;

	jswBuilder["emitUTF8"] = true;
	std::unique_ptr<Json::StreamWriter>jsWriter(jswBuilder.newStreamWriter());
	std::ostringstream os;
	jsWriter->write(msgInfo, &os);
	response = strdup(os.str().c_str());

	return response;
}

void cleanupRequest(JSONREQUEST_INFO* req) {
	/*
	char *topdir;
   char *filepath;//前端请求日志路径
   char *appname;//前端请求的应用日志名称
   char *type; //0静态日志 1动态日志
   char *seg_start;//获取日志起始行
   char *seg_end; //获取日志结束行
	*/
	if (req->topdir) {
		free(req->topdir);
		req->topdir = nullptr;
	}

	if (req->filepath) {
		free(req->filepath);
		req->filepath = nullptr;
	}

	if (req->appname) {
		free(req->appname);
		req->appname = nullptr;
	}

	if (req->type) {
		free(req->type);
		req->type = nullptr;
	}

	if (req->seg_start) {
		free(req->seg_start);
		req->seg_start = nullptr;
	}

	if (req->seg_end) {
		free(req->seg_end);
		req->seg_end = nullptr;
	}
}