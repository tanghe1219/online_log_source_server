#include <stdio.h>
#include <stdlib.h>

#include "../file_include/apollo_safeop.h"
#include "../file_include/apollo_errno.h"
#include "../file_include/apollo_list.h"

VOID  apollo_delete_node(PVOID* node, APOLLO_DELETE_NODE_CALLBACK cb)
{
	if ((NULL == node) || (NULL == *node)) return;
	APOLLO_LIST_HEAD* datanode = *node;

	if (cb) {
		cb(datanode);
	}
	safe_free(datanode);
	*node = NULL;
}

PVOID apollo_create_node(INT32_T length)
{
	PVOID* node = NULL;
	node = calloc(length, 1);
	return node;
}

VOID  apollo_delete_list_node(APOLLO_LIST_HEAD** node, APOLLO_DELETE_NODE_CALLBACK cb)
{
	if ((NULL == node) || (NULL == *node)) return;
	APOLLO_LIST_HEAD* datanode = *node;
	if (cb) {
		cb(datanode->info);
		safe_free(datanode->info);
	}
	safe_free(datanode);
	*node = NULL;
}

VOID  apollo_delete_list(APOLLO_LIST_HEAD** list, APOLLO_DELETE_NODE_CALLBACK cb)
{
	if ((NULL == list) || (NULL == *list)) return;
	APOLLO_LIST_HEAD* listnode = *list;
	while (listnode) {
		APOLLO_LIST_HEAD* nextnode = listnode->next;
		apollo_delete_list_node(&listnode, cb);
		listnode = nextnode;
	}
	*list = NULL;
}

APOLLO_LIST_HEAD* apollo_create_list_node(PVOID info)
{
	APOLLO_LIST_HEAD* node = NULL;
	if (NULL == (node = apollo_create_node(sizeof(APOLLO_LIST_HEAD)))) {
		return node;
	}
	node->info = info;
	return node;
}

VOID  apollo_add_node_to_list(APOLLO_LIST_HEAD** list, APOLLO_LIST_HEAD* node, APOLLO_LIST_FILTER_CALLBACK filter, APOLLO_DELETE_NODE_CALLBACK delnode, APOLLO_LIST_ORDERBY_CALLBACK orderby)
{
	if ((NULL == list) || (NULL == node) || (NULL == node->info)) return;
	if (NULL == *list) {
		*list = node;
		return;
	}

	APOLLO_LIST_HEAD* listnode = *list;
	APOLLO_LIST_HEAD* prevnode = NULL;
	while (listnode) {
		APOLLO_LIST_HEAD* nextnode = listnode->next;
		if (listnode->info) {
			if (filter) {
				if (filter(listnode->info, node->info) == APOLLO_OK) {
					if (prevnode) {
						prevnode->next = node;
					}
					else {
						*list = node;
					}
					node->next = nextnode;
					if (delnode) {
						apollo_delete_list_node((APOLLO_LIST_HEAD**)&listnode, delnode);
					}
					break;
				}
			}
			if (orderby) {
				if (orderby(listnode->info, node->info) == APOLLO_OK) {
					if (prevnode) {
						prevnode->next = node;
					}
					else {
						*list = node;
					}
					node->next = listnode;
					break;
				}
			}
			if (NULL == nextnode) {
				listnode->next = node;
				break;
			}
		}
		if (listnode) prevnode = listnode;
		listnode = nextnode;
	}
}

VOID  apollo_insert_node_to_list_head(APOLLO_LIST_HEAD** list, APOLLO_LIST_HEAD* node)
{
	if ((NULL == list) || (NULL == node) || (NULL == node->info)) return;
	if (NULL == *list) {
		*list = node;
		return;
	}

	APOLLO_LIST_HEAD* listnode = *list;
	*list = node;
	node->next = listnode;
}

INT32_T count_apollo_list(APOLLO_LIST_HEAD* list)
{
	INT32_T  count = 0;
	APOLLO_LIST_HEAD* listnode = list;
	while (listnode) {
		count++;
		listnode = listnode->next;
	}
	return count;
}


VOID  apollo_delete_node_from_list(APOLLO_LIST_HEAD** list, APOLLO_LIST_HEAD* node, APOLLO_LIST_FILTER_CALLBACK filter, APOLLO_DELETE_NODE_CALLBACK delnode)
{
	if ((NULL == list) || (NULL == *list) || (NULL == node) || (NULL == node->info)) return;

	APOLLO_LIST_HEAD* listnode = *list;
	APOLLO_LIST_HEAD* prevnode = NULL;
	while (listnode) {
		APOLLO_LIST_HEAD* nextnode = listnode->next;
		if (listnode->info) {
			if (filter) {
				if (filter(listnode->info, node->info) == APOLLO_OK) {
					if (prevnode) {
						prevnode->next = node;
					}
					else {
						*list = node;
					}
					node->next = nextnode;
					if (delnode) {
						delnode(&listnode);
					}
					break;
				}
			}
		}
		if (listnode) prevnode = listnode;
		listnode = nextnode;
	}
}

RTSTATUS apollo_list_each_entry(APOLLO_LIST_HEAD* list, APOLLO_LIST_DO_CALLBACK dofunc)
{
	APOLLO_LIST_HEAD* listnode = NULL;
	for (listnode = list; listnode; listnode = listnode->next) {
		if (listnode->info) {
			if (dofunc) dofunc(listnode->info);
		}
	}
	return APOLLO_OK;
}

RTSTATUS apollo_deal_each_entry(APOLLO_LIST_HEAD** list, APOLLO_LIST_MASK_CALLBACK maskfunc, APOLLO_DELETE_NODE_CALLBACK delnode, APOLLO_LIST_DO_CALLBACK dofunc)
{
	if (NULL == list) return APOLLO_OK;

	APOLLO_LIST_HEAD* listnode = *list;
	APOLLO_LIST_HEAD* prevnode = NULL;
	while (listnode) {
		APOLLO_LIST_HEAD* nextnode = listnode->next;
		if (listnode->info) {
			if (maskfunc && (maskfunc(listnode->info) != APOLLO_OK)) {
				if (prevnode) {
					prevnode->next = nextnode;
				}
				else {
					*list = nextnode;
				}
				apollo_delete_list_node((APOLLO_LIST_HEAD**)&listnode, delnode);
			}
			else if (dofunc) {
				dofunc(listnode->info);
			}
		}
		if (listnode) prevnode = listnode;
		listnode = nextnode;
	}
	return APOLLO_OK;

}

