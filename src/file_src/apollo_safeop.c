#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include <sys/stat.h>
#include <ctype.h>

#include "../file_include/apollo_errno.h"
#include "../file_include/apollo_safeop.h"

VOID safe_free(PVOID ptr)
{
	if (ptr) {
		free(ptr);
	}
}

INT8_T* safe_strdup(INT8_T* ptr)
{
	if (NULL == ptr) {
		return NULL;
	}

	return strdup(ptr);
}

INT32_T safe_strcmp(INT8_T* ptr1, INT8_T* ptr2)
{
	if ((NULL == ptr1) || (NULL == ptr2)) {
		return -1;
	}

	if (strlen(ptr1) == strlen(ptr2)) {
		if (strcmp(ptr1, ptr2) == 0) {
			return 0;
		}
	}

	return 1;
}


INT32_T safe_my_strcmp(INT8_T* ptr1, INT8_T* ptr2)
{
	if ((NULL == ptr1) || (NULL == ptr2)) {
		return -1;
	}

	if (strlen(ptr1) == strlen(ptr2)) {
		if (strcmp(ptr1, ptr2) < 0) {
			return 0;
		}
		else {
			return 1;
		}
	}

	return 1;
}

RTSTATUS safe_strcasecmp(INT8_T* ptr1, INT8_T* ptr2)
{
	if ((NULL == ptr1) || (NULL == ptr2)) {
		return -1;
	}

	if (strlen(ptr1) == strlen(ptr2)) {
		if (strcasecmp(ptr1, ptr2) == 0) {
			return 0;
		}
	}

	return 1;
}

RTSTATUS safe_memcmp(PVOID ptr1, PVOID ptr2, INT32_T length)
{
	if ((NULL == ptr1) || (NULL == ptr2)) {
		return -1;
	}
	if (memcmp(ptr1, ptr2, length) == 0) {
		return 0;
	}
	return 1;
}

RTSTATUS safe_memcpy(PVOID ptr1, PVOID ptr2, INT32_T length)
{
	if ((NULL == ptr1) || (NULL == ptr2)) {
		return APOLLO_ERRNO_INVALID_ARGUMENTS;
	}
	memcpy(ptr1, ptr2, length);

	return APOLLO_OK;
}

INT32_T safe_strlen(INT8_T* ptr)
{
	if (NULL == ptr) {
		return 0;
	}

	return strlen(ptr);
}


INT8_T* safe_strstr(INT8_T* ptr1, INT8_T* ptr2)
{
	if ((NULL == ptr1) || (NULL == ptr2)) {
		return NULL;
	}
	return strstr(ptr1, ptr2);
}

INT32_T safe_ipcmp(INT8_T* ipfmt1, INT8_T* ipfmt2)
{
	if ((NULL == ipfmt1) || (NULL == ipfmt2)) {
		return -1;
	}

	INT32_T seg1, seg2, seg3, seg4;
	if (sscanf(ipfmt1, "%d.%d.%d.%d", &seg1, &seg2, &seg3, &seg4) != 4) {
		return -1;
	}

	INT32_T seg5, seg6, seg7, seg8;
	if (sscanf(ipfmt1, "%d.%d.%d.%d", &seg5, &seg6, &seg7, &seg8) != 4) {
		return -1;
	}

	INT32_T retcode = 0;
	if (seg1 > seg5) {
		retcode = 1;
	}
	else if (seg1 < seg5) {
		retcode = 2;
	}
	else {
		if (seg2 > seg6) {
			retcode = 1;
		}
		else if (seg2 < seg6) {
			retcode = 2;
		}
		else {
			if (seg3 > seg7) {
				retcode = 1;
			}
			else if (seg3 < seg7) {
				retcode = 2;
			}
			else {
				if (seg4 > seg8) {
					retcode = 1;
				}
				else if (seg4 < seg8) {
					retcode = 2;
				}
			}
		}
	}

	return retcode;
}

RTSTATUS extra_mkdir(INT8_T* dir)
{
	if (NULL == dir) {
		return APOLLO_ERRNO_INVALID_ARGUMENTS;
	}

	INT8_T* splitdir;
	INT8_T* zonedir;
	INT8_T* topdir;

	topdir = dir;
	zonedir = dir;

	INT8_T* localdir = NULL;
	while (NULL != (splitdir = strchr(zonedir, '/'))) {
		if ((splitdir - topdir)) {
			INT32_T length = splitdir - topdir;
			if (NULL == (localdir = (INT8_T*)calloc(length + 1, 1))) {
				return APOLLO_ERRNO_MEM_ALLOCATE_FAIL;
			}
			memcpy(localdir, topdir, splitdir - topdir);
			if (mkdir(localdir, 0777) < 0) {
				if (errno != EEXIST) {
					safe_free(localdir);
					return APOLLO_ERRNO_MKDIR;
				}
			}
			safe_free(localdir);
		}
		zonedir = splitdir + 1;
	}

	if (mkdir(topdir, 0777) < 0) {
		if (errno != EEXIST) {
			return APOLLO_ERRNO_MKDIR;
		}
	}

	return APOLLO_OK;
}

VOID safe_skip_nonchar_and_nonnum_from_string(INT8_T* string)
{
	if (NULL == string) {
		return;
	}

	INT8_T length = strlen(string);
	INT32_T i;
	for (i = 0; i < length; i++) {
		if ((string[i] >= 'a') && (string[i] <= 'z')) {
			continue;
		}
		else if ((string[i] >= 'A') && (string[i] <= 'Z')) {
			continue;
		}
		else if ((string[i] >= '0') && (string[i] <= '9')) {
			continue;
		}
		else {
			string[i] = ' ';
		}
	}
}

INT8_T* safe_strip_special_characters_from_string_head_and_tail(INT8_T* string, INT8_T* characters, INT32_T nchar)
{
	if ((NULL == string) || (NULL == characters)) {
		return string;
	}

	INT32_T i;
	INT32_T j;

	INT32_T length = strlen(string);
	INT8_T* p = string;
	for (i = 0; i < length; i++) {
		BOOL found_strip_char = FALSE;
		for (j = 0; j < nchar; j++) {
			if (*p == characters[j]) {
				*p++ = 0;
				found_strip_char = TRUE;
			}
		}
		if (found_strip_char == FALSE) {
			break;
		}
	}

	for (i = length - 1; i >= 0; i--) {
		BOOL found_strip_char = FALSE;
		for (j = 0; j < nchar; j++) {
			if (string[i] == characters[j]) {
				string[i] = 0;
				found_strip_char = TRUE;
			}
		}
		if (found_strip_char == FALSE) {
			break;
		}
	}

	return p;
}

INT8_T* safe_upper_ptr(INT8_T* ptr)
{
	if (NULL == ptr) return NULL;
	INT32_T length = strlen(ptr);

	INT8_T* outptr = NULL;
	if (NULL == (outptr = safe_strdup(ptr))) {
		return NULL;
	}

	INT32_T i;
	for (i = 0; i < length; i++) {
		outptr[i] = toupper(ptr[i]);
	}

	return outptr;
}

INT8_T* safe_lower_ptr(INT8_T* ptr)
{
	if (NULL == ptr) return NULL;
	INT32_T length = strlen(ptr);

	INT8_T* outptr = NULL;
	if (NULL == (outptr = safe_strdup(ptr))) {
		return NULL;
	}

	INT32_T i;
	for (i = 0; i < length; i++) {
		outptr[i] = tolower(ptr[i]);
	}

	return outptr;
}

