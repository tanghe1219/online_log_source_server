#include "file_operator.h"
#include <sys/types.h> 
#include <unistd.h>
#include <sys/stat.h> 
#include <fcntl.h>
#include <dirent.h>
#include <string.h>
#include "../const.h"
#include <iostream>

namespace logger {

FileOperator::FileOperator() {
    fd_ = -1;
    fp_ = nullptr;
    is_open_ = false;
}

FileOperator::~FileOperator() {
    close();
}

void FileOperator::rewind() {
    if (!is_open_) {
        return;
    }

	switch (open_way_)
	{
    case BINARY_NO_BUFF: {
        lseek(fd_, 0, SEEK_SET);
        break;
    }
    case TEXT_WITH_BUFF: {
        fseek(fp_, 0, SEEK_SET);
        break;
    }
    }
}

bool FileOperator::open(const std::string& file_name, OpenFileWay open_way) {
    full_file_name_ = file_name;
    open_way_ = open_way;

    if (!exists(file_name)) {
        return false;
    }

    switch (open_way_)
    {
    case BINARY_NO_BUFF: {
        fd_ = ::open(full_file_name_.c_str(), O_RDONLY);
        if (fd_ < 0) {
            return false;
        }
        break;
    }

    case TEXT_WITH_BUFF: {
        fp_ = fopen(full_file_name_.c_str(), "rw+");
        if (!fp_) {
            return false;
        }
        break;
    }

    case PIPE_WITH_BUFF: {
        fp_ = popen(full_file_name_.c_str(), "r");
        if (!fp_) {
            return false;
        }
        break;
    }
    }

    if (fp_) {
        ::setvbuf(fp_, NULL, _IONBF, 0);
    }
    
    is_open_ = true;
    return true;
}

bool FileOperator::close() {
    switch (open_way_)
    {
    case BINARY_NO_BUFF: {
         if (fd_ > 0) {
            ::close(fd_);
        }
        break;
    }

    case TEXT_WITH_BUFF: {
        if (fp_) {
            fclose(fp_);
        }
        break;
    }

    case PIPE_WITH_BUFF: {
        if (fp_) {
            pclose(fp_);
        }
        break;
    }
    }
    
    is_open_ = false;
}

bool FileOperator::readLine(size_t& last_line, size_t begin_line, size_t end_line, std::string& content) {
    int n_lines = end_line - begin_line + 1;
    size_t bytes_read = 0;
    size_t bytes_to_skip = 0;

    if (n_lines == 0) {
        return true;
    }

    if (end_line < begin_line) {
        // ....
        return false;
    }

    if (open_way_ == BINARY_NO_BUFF) {
        bool ret = true;
        // 跳过begin_line
        if (begin_line > last_line) {  // 将指针后移begin_line - last_line行
            ret = rewindAfter(begin_line - last_line);
        } else if (begin_line < last_line) { // 将指针前移到begin_line行
            ret = rewindAfter(begin_line);
        }

        if (!ret) {
            // errno = 读文件错误
            return false;
        }

        last_line = end_line;
        return getLinesContent(n_lines, content);

    } else {
        // 每次读取一个block 4096字节
        const int BLOCK_SIZE = 4096;  // 4K 一块
        char block[BLOCK_SIZE] = {0};
        // FILE带缓冲区的读写
        size_t curr_line = 0;
        if (last_line == begin_line) {
            curr_line = begin_line;
        }

        ::rewind(fp_);
        while (curr_line < begin_line && fgets(block, BLOCK_SIZE, fp_) != NULL) {
            curr_line ++;
        }

        memset(block, 0x0, sizeof(sizeof(block)));
        while (curr_line <= end_line && fgets(block, BLOCK_SIZE, fp_) != NULL) {
            curr_line ++;
            content += block;
            memset(block, 0x0, sizeof(sizeof(block)));
        }

        if (curr_line < end_line) {
            last_line = curr_line;
            end_line = curr_line;
        }
    }

    return true;
}

bool FileOperator::readEndOfLine(size_t& last_line, size_t& last_pos, size_t read_line, std::string& content) {
    size_t begin_line = 0;
    size_t total_line = 0;
    if (!getTotalLines(total_line)) {
        return false;
    }

    // 没有产生新的内容
    if (last_line == total_line) {
        return true;
    }

    if (last_line == 0 && last_pos == 0) {  // 第一次打开
        rewindAfter(total_line - read_line);

        if (!getLinesContent(read_line, content)) {
            return false;
        }
    } else {
        if (open_way_ == BINARY_NO_BUFF) {
            lseek(fd_, last_pos, SEEK_SET);
        }
        else {
            fseek(fp_, last_pos, SEEK_SET);
        }
        
        if (!getLinesContent(total_line - last_line, content)) {
            return false;
        }
    }
    
    last_line = total_line;

    if (open_way_ == BINARY_NO_BUFF) {
        off_t curr_pos = lseek(fd_, 0, SEEK_CUR);
        last_pos = (curr_pos > 0) ? curr_pos : 0;
    } else {
        long curr_pos = ftell(fp_);
        last_pos = (curr_pos > 0) ? curr_pos : 0;
    }
    
    return true;
}

bool FileOperator::read(const size_t begin, const size_t end, std::string& content) {
    if (open_way_ == PIPE_WITH_BUFF) {
        return false;
    }

    size_t read_len = end - begin;

    off_t curr_pos = lseek(fd_, 0, SEEK_CUR);
    lseek(fd_, begin - curr_pos, SEEK_CUR);

    content.resize(read_len);
    ssize_t res = ::read(fd_, &content[0], read_len);
    if (res < 0) {
        return false;
    }

    return true;
}

bool FileOperator::write(const std::string& content) {
    if (content.empty()) {
        return true;
    }

    if (open_way_ == BINARY_NO_BUFF) {
        lseek(fd_, 0, SEEK_END);
        ssize_t write_len = 0, total_write = content.size();
        
        while (1) {
            write_len = ::write(fd_, &content[0] + write_len, total_write);
            if (write_len < 0) {
                return false;
            }

            if (write_len < total_write) {
                total_write -= write_len;
            } else {
                break;
            }
        }
    
    } else {

    }

    return true;
}

bool FileOperator::rewindAfter(size_t lines) {
    const int BLOCK_SIZE = 4096;  // 4K 一块
    char block[BLOCK_SIZE] = {0};

    if (open_way_ == BINARY_NO_BUFF) {
        ssize_t bytes_read = 0;
        ssize_t bytes_to_skip = 0;
        ssize_t bytes_stop = 0;
        
        // 前提文件指针是在正确的地方
        off_t last_pos = lseek(fd_, 0, SEEK_CUR);
        lseek(fd_, 0, SEEK_SET);

        while (lines) {
            bytes_to_skip = 0;
            memset(block, 0x0, sizeof(block));
            bytes_read = ::read(fd_, block, BLOCK_SIZE);
            if (bytes_read == 0) { /* EOF */
                return true;
            }
            
            if (bytes_read < 0) /* error */ {
                return false;
            }

            while (bytes_to_skip < bytes_read) {
                if (block[bytes_to_skip++] == '\n') {
                    if (lines == 0) {
                        break;
                    }
                    // bytes_stop += bytes_to_skip;
                    lines--;
                }
            }

            bytes_stop += bytes_to_skip;
        }

        off_t new_pos = bytes_stop + last_pos;
        lseek(fd_, new_pos, SEEK_SET);
    } else {
        while (fgets(block, BLOCK_SIZE, fp_) != NULL && lines-- != 0) {
            ;
        }
    }
    
    return true;
}

bool FileOperator::getLinesContent(ssize_t lines, std::string& content) {
    const int BLOCK_SIZE = 4096;  // 4K 一块
    char block[BLOCK_SIZE] = {0};

    if (open_way_ == BINARY_NO_BUFF) {
        ssize_t bytes_read = 0;
        ssize_t bytes_stop = 0;
        ssize_t bytes_to_skip = 0, last_bytes_endline = 0;

        off_t last_pos = lseek(fd_, 0, SEEK_CUR);
        // lseek(fd_, 0, SEEK_SET);

        while (lines) {
            bytes_to_skip = 0;
            last_bytes_endline = 0;
            memset(block, 0x0, sizeof(block));
            bytes_read = ::read(fd_, block, BLOCK_SIZE);
            if (bytes_read == 0) { /* EOF */
                return true;
            }
            
            if (bytes_read < 0) /* error */ {
                return false;
            }

            while (bytes_to_skip < bytes_read) {
                if (block[bytes_to_skip++] == '\n') {
                    if (lines == 0) {
                        break;
                    }

                    lines --;

                    // bytes_stop += bytes_to_skip;
                    std::string item(block + last_bytes_endline, bytes_to_skip - last_bytes_endline);
                    if (!item.empty()) {
                        content += item;
                    } 
                    // content += "\n";
                    last_bytes_endline = bytes_to_skip;
                }

                if (lines == 0) {
                    break;
                }
            }

            bytes_stop += bytes_to_skip;
        }

        off_t new_pos = bytes_stop + last_pos;
        lseek(fd_, new_pos, SEEK_SET);

    } else {
        while (fgets(block, BLOCK_SIZE, fp_) != NULL && lines-- != 0) {
            content += block;
            memset(block, 0x0, BLOCK_SIZE);
        }
    }

    return true;
}

bool FileOperator::getLinesContent(ssize_t lines, std::vector<std::string>& content) {
	const int BLOCK_SIZE = 4096;  // 4K 一块
	char block[BLOCK_SIZE] = { 0 };

	if (open_way_ == BINARY_NO_BUFF) {
		ssize_t bytes_read = 0;
		ssize_t bytes_stop = 0;
		ssize_t bytes_to_skip = 0, last_bytes_endline = 0;

		off_t last_pos = lseek(fd_, 0, SEEK_CUR);
		// lseek(fd_, 0, SEEK_SET);

		while (lines) {
			bytes_to_skip = 0;
			last_bytes_endline = 0;
			memset(block, 0x0, sizeof(block));
			bytes_read = ::read(fd_, block, BLOCK_SIZE);
			if (bytes_read == 0) { /* EOF */
				return true;
			}

			if (bytes_read < 0) /* error */ {
				return false;
			}

			while (bytes_to_skip < bytes_read) {
				if (block[bytes_to_skip++] == '\n') {
					if (lines == 0) {
						break;
					}

					lines--;

					// bytes_stop += bytes_to_skip;
					std::string item(block + last_bytes_endline, bytes_to_skip - last_bytes_endline);
					if (!item.empty()) {
                        content.push_back(item);
					}
					last_bytes_endline = bytes_to_skip;
				}

				if (lines == 0) {
					break;
				}
			}

			bytes_stop += bytes_to_skip;
		}

		off_t new_pos = bytes_stop + last_pos;
		lseek(fd_, new_pos, SEEK_SET);

	}
	else {
		while (fgets(block, BLOCK_SIZE, fp_) != NULL && lines-- != 0) {
            content.push_back(block);
			memset(block, 0x0, BLOCK_SIZE);
		}
	}

	return true;
}

bool FileOperator::getTotalLines(size_t &lines) {
    const int BLOCK_SIZE = 4096;  // 4K 一块
    char block[BLOCK_SIZE] = {0};

    if (open_way_ == BINARY_NO_BUFF) {
        ssize_t  bytes_read = 0;
        ssize_t  bytes_to_skip = 0;
        off_t last_pos = lseek(fd_, 0, SEEK_CUR);
        lseek(fd_, 0, SEEK_SET);

        do {
            bytes_to_skip = 0;
            bytes_read = 0;
            memset(block, 0x0, sizeof(block));
            bytes_read = ::read(fd_, block, BLOCK_SIZE);
            if (bytes_read == 0) { /* EOF */
                lseek(fd_, last_pos, SEEK_SET);
                return true;
            }
            
            if (bytes_read < 0) /* error */ {
                lseek(fd_, last_pos, SEEK_SET);
                return false;
            }

            while (bytes_to_skip <= bytes_read) {
                if (block[bytes_to_skip++] == '\n') {
                    lines++;
                }
            }

        } while(true);

        lseek(fd_, last_pos, SEEK_SET);

    } else {
        long last_pos = ftell(fp_);
        ::rewind(fp_);
        while (fgets(block, BLOCK_SIZE, fp_) != NULL) {
            lines ++;
        }
        fseek(fp_, last_pos, SEEK_SET);
    }
    
    return true;
}

bool FileOperator::exists(const std::string& full_file_name) {
    if (access(full_file_name.c_str(), F_OK) == 0) {
        return true;
    }

    return false;
}

    // 本文件是正在加载的文件，动态变化
bool FileOperator::isDynamic(const std::string& file_name) {
    if (file_name.empty()) {
        return false;
    }

    DIR *dir = opendir("/proc");
    if (dir == NULL) {
        return false;
    }

    struct dirent *dirent;
    char proc_path[256] = "0";
    char proc_sub_path[256] = "0";
    char name_buff[256] = "0";
    while (NULL != (dirent = readdir(dir))) { 
        if (0 == strcmp(".", dirent->d_name) || 0 == strcmp("..", dirent->d_name)) {
            continue;
        }
        
        DIR *fdir = NULL;
        memset(proc_path, 0x0, sizeof(proc_path));
        sprintf(proc_path, "/proc/%s/fd/", dirent->d_name);
        fdir = opendir(proc_path);
        if (fdir == NULL) {
            continue;
        }

        struct dirent *fdirent;
        while ((fdirent = readdir(fdir)) != NULL) {
            if (strcmp(".", fdirent->d_name) == 0 || 
                strcmp("..", fdirent->d_name) == 0) {
                continue;
            }
            
            memset(proc_sub_path, 0x0, sizeof(proc_sub_path));
            sprintf(proc_sub_path, "%s/%s", proc_path, fdirent->d_name);

            memset(name_buff, 0, sizeof(name_buff));
            readlink(proc_sub_path, name_buff, sizeof(name_buff));
            if (strcmp(proc_sub_path, name_buff) == 0) {
                return true;
            }
        }

        closedir(fdir);
    }

    closedir(dir);
    return false;
}

FileType FileOperator::getFileType(const std::string& full_file_name) {
    if (!exists(full_file_name)) {
        return NONE_TYPE;
    }

    char buff[1024] = {0};
    sprintf(buff, "file %s | cut -d':' -f2 | cut -d',' -f1", full_file_name.c_str());

    FILE *cmd_pipe = popen(buff, "r"); 
    if (!cmd_pipe) {
        return NONE_TYPE;
    }

    char data[32] = {0};
    size_t len = fread(data, sizeof(data), 1, cmd_pipe);
    if (len < 0) {
        return NONE_TYPE;
    }

    pclose(cmd_pipe);
    data[len] = 0;

    if (strncasecmp(data, ASCII_TEXT_TYPE.c_str(), ASCII_TEXT_TYPE.size()) == 0 ||
        strncasecmp(data, UTF8_TEXT_TYPE.c_str(), UTF8_TEXT_TYPE.size()) == 0) {
        return TEXT_FILE;
    }

    if (strncasecmp(data, ELF_TYPE.c_str(), ELF_TYPE.size()) == 0 ||
        strncasecmp(data, JPEG_TYPE.c_str(), JPEG_TYPE.size()) == 0) {
        return BINARY;
    }

    if (strncasecmp(data, DIRECTORY_TYPE.c_str(), DIRECTORY_TYPE.size()) == 0) {
        return DIRECTORY;
    }
}

size_t FileOperator::getFileSize(const std::string& full_file_name) {
    struct stat buf; 
    if (stat(full_file_name.c_str(), &buf) == 0) {
        return (size_t)buf.st_size;
    } 

    return 0;
}

size_t FileOperator::getFileLines(const std::string& full_file_name) {
    return 0;
}

bool FileOperator::getFileName(const std::string& dir_path, 
    const std::string& file_name, 
    std::string& full_file_path,
    int max_level) {

    if (max_level < 0) {
        return true;
    }

    DIR *dir = opendir(dir_path.c_str());
    if (dir == NULL) {
        return false;
    }

    struct dirent *ent;
    while ((ent = readdir(dir)) != NULL) {
        if (strcmp(ent->d_name,".") == 0 ||
            strcmp(ent->d_name,"..") == 0) {
            continue;
        }

        if (strcmp(ent->d_name, file_name.c_str()) == 0) {
            full_file_path += dir_path;
            full_file_path += "/";
            full_file_path += file_name;
            full_file_path += "\n";
            continue;
        }

        struct stat st;
        std::string deep_path = dir_path + "/" + ent->d_name;
        stat(deep_path.c_str(), &st); 

        if (S_ISDIR(st.st_mode)) {
            getFileName(deep_path, file_name, full_file_path, max_level - 1);
        }
    }

    closedir(dir);

    return true;
}

};