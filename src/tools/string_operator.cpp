#include "string_operator.h"

namespace logger {

	std::vector<std::string> StringOperator::split(const std::string& src, char token) {
		std::vector<std::string> vec;

		if (src.empty()) {
			return vec;
		}

		size_t start_pos = 0;

		size_t curr_pos = 0;
		while (curr_pos < src.size()) {
			while (curr_pos < src.size() && src[curr_pos] == token) {
				curr_pos++;
			}

			if (curr_pos >= src.size()) {
				break;
			}

			start_pos = curr_pos;
			while (curr_pos < src.size() && src[curr_pos] != token) {
				curr_pos++;
			}

			std::string item;
			item.assign(src, start_pos, curr_pos - start_pos);
			vec.push_back(item);
		}

		return vec;
	}

};