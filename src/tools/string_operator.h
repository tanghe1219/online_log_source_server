#ifndef _STRING_OPERATOR_H_
#define _STRING_OPERATOR_H_

#include <string>
#include <vector>

namespace logger {
	
	class StringOperator {

	public:
		static std::vector<std::string> split(const std::string& src, char token);
		inline static std::string DoubleToStr(double value) {
			char data[16] = { 0 };
			sprintf(data, "%.3f", value);
			data[15] = 0;
			return std::string(data);
		}
	};

};

#endif // _STRING_OPERATOR_H_