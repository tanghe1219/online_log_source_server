#ifndef _FILE_OPERATOR_H_
#define _FILE_OPERATOR_H_

#include <string>
#include <vector>
#include "../struct.h"

namespace logger {

class FileOperator {
public:
    FileOperator();
    ~FileOperator();

    void rewind();
    bool open(const std::string& file_name, OpenFileWay open_way = BINARY_NO_BUFF);
    bool isOpen() { return is_open_; };
    bool close();
    bool readLine(size_t& last_line, size_t begin_line, size_t end_line, std::string& content);
    bool read(const size_t begin, const size_t end, std::string& content);
    bool write(const std::string& content);
    bool rewindAfter(size_t lines);
	bool getLinesContent(ssize_t lines, std::string& content);
	bool getLinesContent(ssize_t lines, std::vector<std::string>& content);
    bool getTotalLines(size_t &lines);
    bool readEndOfLine(size_t& last_line, size_t& last_pos, size_t read_line, std::string& content);

    static bool exists(const std::string& full_file_name);

    // 本文件是正在加载的文件，动态变化
    static bool isDynamic(const std::string& full_file_name);
    static FileType getFileType(const std::string& full_file_name);
    static size_t getFileSize(const std::string& full_file_name);
    static size_t getFileLines(const std::string& full_file_name);
    static bool getFileName(const std::string& dir_path, 
        const std::string& file_name,
        std::string& full_file_path,
        int max_level = 16);

private:
    std::string full_file_name_;
    int fd_;
    FILE* fp_;
    OpenFileWay open_way_;
    bool is_open_;
};

};

#endif // !_FILE_OPERATOR_H_