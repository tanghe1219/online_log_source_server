#ifndef _STRUCT_H_
#define _STRUCT_H_

#include <map>
#include <string>

namespace logger {

struct NetFileInfo {
    std::string file_name;
    std::string path_file_name;
    size_t file_size;
    ssize_t seg_start;
    ssize_t seg_end;
    bool is_dynamic;
    bool is_default;  // 给前端是否是系统配置，不能删
    bool is_opened;
    size_t last_line;
    size_t last_pos;
    void* arg;

    // 文件缓存    

    NetFileInfo() {
        reset();
    }

    void reset() {
        file_name = "";
        path_file_name = "";
        file_size = 0;
        is_dynamic = false;
        is_default = false;
        seg_start = 0;
        seg_end = -1;
        last_line = 0;  
        last_pos = 0;
        is_opened = false;
    }
};

enum FileType {
    NONE_TYPE,
    TEXT_FILE,
    BINARY, // ELF, JPEG
    DIRECTORY,
};

enum ErrorCode {
    NONE_ERROR,
    UNKNOW_FILE_ERR = 1,
    SERVER_INTER_ERR = 2,
    NEED_MUST_PARAM_ERR = 3,
    FILE_EXIST_ERR = 4,
    FILE_NO_EXIST_ERR = 5,
    QUERY_FILE_FAILED_ERR = 6,
    FILE_LIST_EMPTY_ERR = 7,
    FILE_OPEN_FAILED_ERR = 8,
};

enum OpenFileWay {
    BINARY_NO_BUFF,
    TEXT_WITH_BUFF,
    PIPE_WITH_BUFF,
};

};

#endif // !_STRUCT_H_