/**
 *
 * filename: connection.h
 */

#ifndef _CONNECTION_H_
#define _CONNECTION_H_
#include <functional>
#include <stdlib.h>
#include "websocket.h"
#include "muduo/net/TcpConnection.h"

namespace logger {

// typedef void(*websocket_cb)(void*, std::string);
typedef std::function<void (void*, std::string)> websocket_cb;

typedef struct {
	websocket_cb cb;
	void *cbarg;
	std::string data;
} ws_cb_unit;


 struct websocket_connection {
	std::shared_ptr<muduo::net::TcpConnection> conn;
	std::string ws_req_str;
	std::string ws_resp_str;
	enum Step step;
	uint64_t ntoread;
	frame_t frame; //current frame
	ws_req_t request;
	ws_cb_unit handshake_cb_unit;
	ws_cb_unit frame_recv_cb_unit;
	ws_cb_unit write_cb_unit;
	ws_cb_unit close_cb_unit;
	ws_cb_unit ping_cb_unit;

	websocket_connection() {
		conn.reset();
		ws_req_str = "";
		ws_resp_str = "";
		step = ZERO;
		ntoread = 0;
		//frame = frame_new();
		handshake_cb_unit.cb = NULL;
		handshake_cb_unit.cbarg = NULL;
		frame_recv_cb_unit.cb = NULL;
		frame_recv_cb_unit.cbarg = NULL;
		write_cb_unit.cb = NULL;
		write_cb_unit.cbarg = NULL;
		close_cb_unit.cb = NULL;
		close_cb_unit.cbarg = NULL;
		ping_cb_unit.cb = NULL;
		ping_cb_unit.cbarg = NULL;
	}
} ;

typedef websocket_connection ws_conn_t;


//callback type
enum CBTYPE {
	HANDSHAKE,
	FRAME_RECV,
	WRITE,
	CLOSE,
	PING
};

//
// following functions are for library-users
//
//create a websocket connection
ws_conn_t *ws_conn_new();


//destroy a websocket connection
void ws_conn_free(ws_conn_t *conn);


//set callback
//MUST set: frame_read_cb, write_cb, close_cb
void ws_conn_setcb(ws_conn_t *conn, enum CBTYPE cbtype, websocket_cb cb, void *cbarg);


//websocket serve start
void ws_serve_start(ws_conn_t *conn);


//websocket serve exit
void ws_serve_exit(ws_conn_t *conn);


//send a frame
int32_t send_a_frame(ws_conn_t *conn, const frame_buffer_t *fb);

//
// following functions are for internal
//
//accept the websocket request
void accept_websocket_request(ws_conn_t *conn);


//respond the websocket request
void respond_websocket_request(ws_conn_t *conn);


//receive a frame
void frame_recv_loop(ws_conn_t *conn);


//request read callback
void request_read_cb(const muduo::net::TcpConnectionPtr& conn, muduo::net::Buffer* buff, muduo::Timestamp);


//response write callback
void response_write_cb(const muduo::net::TcpConnectionPtr& conn, muduo::net::Buffer* buff, muduo::Timestamp);


//frame read callback
void frame_read_cb(const muduo::net::TcpConnectionPtr& conn, muduo::net::Buffer* buff, muduo::Timestamp);


//websocket write callback
//void write_cb(struct bufferevent *bev, void *ctx);


//connection close callback
//void close_cb(struct bufferevent *bev, short what, void *ctx);

}

#endif 