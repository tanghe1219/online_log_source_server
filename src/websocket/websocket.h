/**
 *
 * filename: websocket.h
 */
#ifndef _WEBSOCKET_H_
#define _WEBSOCKET_H_

#include "stdio.h"
#include "string.h"
#include "tools.h"
#include "frame.h"
//#include "openssl/sha.h"
#include "base64.h"
#include <iostream>
#include <string>
#include <vector>

namespace logger {

//WebSocket request
typedef struct WebsocketRequest {
	std::string req;
	std::string method;
	std::string uri;
	std::string http_ver;
	std::string connection;
	std::string upgrade;
	std::string host;
	std::string origin;
	std::string cookie;
	std::string sec_websocket_key;
	std::string sec_websocket_version;
} ws_req_t;


//WebSocket response
typedef struct WebsocketResponse {
	std::string resp;
	std::string date;
	std::string connection;
	std::string server;
	std::string upgrade;
	std::string access_control_allow_origin;
	std::string access_control_allow_credentials;
	std::string sec_websocket_accept;
	std::string access_control_allow_headers;
} ws_resp_t;


//steps of receiving a frame
enum Step {
	ZERO,  //before websocket handshake
	ONE,   //0-2 bytes, fin, opcode, mask, payload length
	TWO,   //extended payload length
	THREE, //masking-key
	FOUR,  //payload data
	UNKNOWN
};


//parse websocket request
int32_t parse_websocket_request(const char *src, ws_req_t *ws_req);


//print websocket request
void print_websocket_request(const ws_req_t *ws_req);


//generate websocket response
std::string generate_websocket_response(const ws_req_t *ws_req);


//generate websocket key
std::string generate_key(const std::string &key);


//parse fin, opcode, mask, payload len
int32_t parse_frame_header(const char *buf, frame_t *frame);


//unmask payload-data
int32_t unmask_payload_data(frame_t *frame);
int32_t unmask_payload_data(const char *masking_key, char *payload_data, uint32_t payload_len);

};

#endif
