/**
 *
 * filename: frame.h
 */
#ifndef _FRAME_H_
#define _FRAME_H_

#include <string.h>
#include <iostream>
#include "tools.h"

namespace logger {

//frame buffer
typedef struct FrameBuffer {
	std::string data;
	uint64_t len;
} frame_buffer_t;


//frame
struct Frame {
	uint8_t fin;
	uint8_t opcode;
	uint8_t mask;
	uint64_t payload_len;
	unsigned char masking_key[4];
	std::string payload_data;

	Frame() {
		fin = 0;
		opcode = 0;
		mask = 0;
		payload_len = 0;
		memset(masking_key, 0x0, 4 * sizeof(masking_key));
	}
} ;

typedef Frame frame_t;


frame_t *frame_new();


void frame_free(frame_t *frame);


bool is_frame_valid(const frame_t *frame);


bool frame_buffer_new(uint8_t fin,
		uint8_t opcode,
		uint64_t payload_len,
		const char *payload_data,
		frame_buffer_t& fb
	);


bool frame_buffer_new(const frame_t *frame, frame_buffer_t& fb);


void frame_buffer_free(frame_buffer_t *fb);


void print_frame_info(const frame_buffer_t *fb);

};

#endif
