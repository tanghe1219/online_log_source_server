#include "connection.h"

using namespace muduo;
using namespace muduo::net;

namespace logger {

//create a websocket connection
ws_conn_t *ws_conn_new() {
	ws_conn_t *conn = new ws_conn_t;
	if (conn) {
		conn->conn.reset();
		conn->ws_req_str = "";
		conn->ws_resp_str = "";
		conn->step = ZERO;
		conn->ntoread = 0;
		//conn->frame = frame_new();
		conn->handshake_cb_unit.cb = NULL;
		conn->handshake_cb_unit.cbarg = NULL;
		conn->frame_recv_cb_unit.cb = NULL;
		conn->frame_recv_cb_unit.cbarg = NULL;
		conn->write_cb_unit.cb = NULL;
		conn->write_cb_unit.cbarg = NULL;
		conn->close_cb_unit.cb = NULL;
		conn->close_cb_unit.cbarg = NULL;
		conn->ping_cb_unit.cb = NULL;
		conn->ping_cb_unit.cbarg = NULL;
	}
	return conn;
}


//destroy a websocket connection
void ws_conn_free(ws_conn_t *conn) {
	if (conn) {
		delete conn;
		conn = nullptr;
	}
}

//websocket serve start
void ws_serve_start(ws_conn_t *conn) {
	if (conn && conn->conn.get()) {
		accept_websocket_request(conn);
	} else {
		ws_serve_exit(conn);
	}
}

//websocket serve exit
void ws_serve_exit(ws_conn_t *conn) {
	if (conn) {
		if (conn->close_cb_unit.cb) {
			websocket_cb cb = conn->close_cb_unit.cb;
			void *cbarg = conn->close_cb_unit.cbarg;
			cb(cbarg, "");
		}
	}
}

//accept the websocket request
void accept_websocket_request(ws_conn_t *conn) {
	if (conn && conn->conn.get()) {
		//read websocket request
		conn->conn->setMessageCallback(request_read_cb);
		conn->conn->startRead();
	} else {
		ws_serve_exit(conn);
	}
}

//respond the websocket request
void respond_websocket_request(ws_conn_t *conn) {
	if (conn && conn->conn.get()) {
		// again
		if (parse_websocket_request(conn->ws_req_str.c_str(), &conn->request) != 0)  { //parse request
			return;
		}

		conn->ws_resp_str = generate_websocket_response(&conn->request); //generate response
		if (!conn->ws_resp_str.empty()) {
			conn->conn->send(conn->ws_resp_str);
		} 

		conn->conn->setMessageCallback(response_write_cb);
		conn->conn->startRead();

	} else {
		ws_serve_exit(conn);
	}
}

//request read callback
void request_read_cb(const TcpConnectionPtr& conn, Buffer* buff, Timestamp) {
	ws_conn_t* context = boost::any_cast<ws_conn_t>(conn->getMutableContext());
	if (context) {
		size_t n = buff->readableBytes();
		std::string content(buff->peek(), n);

		//receive request completely
		if (n >= 4 && content.substr(n - 4) == "\r\n\r\n") {
			context->ws_req_str.assign(buff->peek(), n);
			context->conn->stopRead();
			respond_websocket_request(context); //send websocket response
			buff->retrieveAll();
		}
	} else {
		ws_serve_exit(context);
	}
}

//response write callback
void response_write_cb(const TcpConnectionPtr& conn, Buffer* buff, Timestamp timestamp) {
	ws_conn_t* context = boost::any_cast<ws_conn_t>(conn->getMutableContext());
	if (context) {
		if (context->handshake_cb_unit.cb) {
			websocket_cb cb = context->handshake_cb_unit.cb;
			void *cbarg = context->handshake_cb_unit.cbarg;
			cb(cbarg, buff->retrieveAllAsString());
		}

		// printf("req: %s \n", context->ws_req_str.c_str());
		// printf("rsp: %s \n", context->ws_resp_str.c_str());

		// frame_recv_loop(context); //frame receive loop
		context->step = ONE;
		context->ntoread = 2;
		frame_read_cb(conn, buff, timestamp);
		// context->conn->setMessageCallback(frame_read_cb);
		// context->conn->startRead();
	} else {
		ws_serve_exit(context);
	}
}

//send a frame
int32_t send_a_frame(ws_conn_t *conn, const frame_buffer_t *fb) {
	if (conn && conn->conn.get()) {
		conn->conn->send(fb->data.c_str(), fb->len);
	}
	
	return 0;
}

void frame_recv_loop(ws_conn_t *conn) {
	if (conn && conn->conn.get()) {
		conn->step = ONE;
		conn->ntoread = 2;
		conn->conn->setMessageCallback(frame_read_cb);
		conn->conn->startRead();
	} else {
		ws_serve_exit(conn);
	}
}

void frame_read_cb(const TcpConnectionPtr& conn, Buffer* buff, Timestamp) {
	ws_conn_t* context = boost::any_cast<ws_conn_t>(conn->getMutableContext());
	if (!context) {
		ws_serve_exit(context);
		return;
	}

	bool end = false;
	do {

	if (buff->readableBytes() < context->ntoread) {
		return;
	}

	char tmp[context->ntoread];
	memcpy(tmp, buff->peek(), context->ntoread);
	// std::cout << "no_of_read: " << context->ntoread << std::endl;
	buff->retrieve(context->ntoread);

	switch (context->step) {
	case ONE:
	{
		// printf("---- STEP 1 ----\n");
		//parse header
		if (parse_frame_header(tmp, &context->frame) == 0) {
			// printf("FIN         = %lu\n", context->frame.fin);
			// printf("OPCODE      = %lu\n", context->frame.opcode);
			// printf("MASK        = %lu\n", context->frame.mask);
			// printf("PAYLOAD_LEN = %lu\n", context->frame.payload_len);
			//payload_len is [0, 127]
			if (context->frame.payload_len <= 125) {
				context->step = THREE;
				context->ntoread = 4;
			} else if (context->frame.payload_len == 126) {
				context->step = TWO;
				context->ntoread = 2;
			} else if (context->frame.payload_len == 127) {
				context->step = TWO;
				context->ntoread = 8;
			}
		}
		//TODO
		//validate frame header
		if (!is_frame_valid(&context->frame)) {
			std::cout << "invalid >" << std::endl;
			return;
		}
		break;
	}

	case TWO:
	{
		// printf("---- STEP 2 ----\n");
		if (context->frame.payload_len == 126) {
			context->frame.payload_len = ntohs(*(uint16_t*)tmp);
			//printf("PAYLOAD_LEN = %lu\n", context->frame.payload_len);
		} else if (context->frame.payload_len == 127) {
			context->frame.payload_len = myntohll(*(uint64_t*)tmp);
			//printf("PAYLOAD_LEN = %llu\n", context->frame.payload_len);
		}
		context->step = THREE;
		context->ntoread = 4;
		// bufferevent_setwatermark(bev, EV_READ, conn->ntoread, conn->ntoread);
		break;
	}

	case THREE:
	{
		// printf("---- STEP 3 ----\n");
		memcpy(context->frame.masking_key, tmp, context->ntoread);
		if (context->frame.payload_len > 0) {
			context->step = FOUR;
			context->ntoread = context->frame.payload_len;
			// bufferevent_setwatermark(bev, EV_READ, conn->ntoread, conn->ntoread);
		} else if (context->frame.payload_len == 0) {
			/*recv a whole frame*/
			if (context->frame.mask == 0) {
				//recv an unmasked frame
			}
			if (context->frame.fin == 1 && context->frame.opcode == 0x8) {
				//0x8 denotes a connection close
				frame_buffer_t fb;
				frame_buffer_new(1, 8, context->frame.payload_len, tmp, fb);
				send_a_frame(context, &fb);
				// printf("step3 - send a close frame\n");
				ws_serve_exit(context);
				end = true;
				break;
			} else if (context->frame.fin == 1 && context->frame.opcode == 0x9) {
				//0x9 denotes a ping
				//TODO
				//make a pong
			} else {
				//execute custom operation
				/*if (context->frame_recv_cb_unit.cb) {
					websocket_cb cb = context->frame_recv_cb_unit.cb;
					void *cbarg = context->frame_recv_cb_unit.cbarg;
					cb(cbarg, buff->retrieveAllAsString());
					end = true;
					break;
				}*/
			}

			context->step = ONE;
			context->ntoread = 2;
		}
		break;
	}

	case FOUR:
	{
		//printf("---- STEP 4 ----\n");
		if (context->frame.payload_len > 0) {
			context->frame.payload_data.assign(tmp, context->frame.payload_len);
			unmask_payload_data(&context->frame);
		}

		/*recv a whole frame*/
		if (context->frame.fin == 1 && context->frame.opcode == 0x8) {
			//0x8 denotes a connection close
			frame_buffer_t fb;
			frame_buffer_new(1, 8, 0, nullptr, fb);
			send_a_frame(context, &fb);
			// printf("step4 - send a close frame\n");
			break;
		} else if (context->frame.fin == 1 && context->frame.opcode == 0x9) {
			//0x9 denotes a ping
			//TODO
			//make a pong
		} else {
			//execute custom operation
			if (context->frame_recv_cb_unit.cb) {
				websocket_cb cb = context->frame_recv_cb_unit.cb;
				void *cbarg = context->frame_recv_cb_unit.cbarg;
				cb(cbarg, "");
				end = true;
			}
		}

		if (context->frame.opcode == 0x1) { //0x1 denotes a text frame
		}
		if (context->frame.opcode == 0x2) { //0x1 denotes a binary frame
		}

		context->step = ONE;
		context->ntoread = 2;
		break;
	}

	default:
		std::cout << "Unknow Type: " << std::endl;
		end = true;
		break;
	}

	} while (!end);

}

void ws_conn_setcb(ws_conn_t *conn, enum CBTYPE cbtype, websocket_cb cb, void *cbarg) {
	if (conn) {
		switch (cbtype) {
		case HANDSHAKE:
			conn->handshake_cb_unit.cb = cb;
			conn->handshake_cb_unit.cbarg = cbarg;
			break;
		case FRAME_RECV:
			conn->frame_recv_cb_unit.cb = cb;
			conn->frame_recv_cb_unit.cbarg = cbarg;
			break;
		case WRITE:
			conn->write_cb_unit.cb = cb;
			conn->write_cb_unit.cbarg = cbarg;
			break;
		case CLOSE:
			conn->close_cb_unit.cb = cb;
			conn->close_cb_unit.cbarg = cbarg;
			break;
		case PING:
			conn->ping_cb_unit.cb = cb;
			conn->ping_cb_unit.cbarg = cbarg;
			break;
		default:
			break;
		}
	}
}

// void write_cb(struct bufferevent *bev, void *ctx) {
// 	ws_conn_t *conn = (ws_conn_t*)ctx;
// 	if (conn) {
// 		if (conn->write_cb_unit.cb) {
// 			websocket_cb cb = conn->write_cb_unit.cb;
// 			void *cbarg = conn->write_cb_unit.cbarg;
// 			cb(cbarg);
// 		}
// 	}
// }

// void close_cb(struct bufferevent *bev, short what, void *ctx) {
// 	ws_serve_exit((ws_conn_t*)ctx);
// }

};
