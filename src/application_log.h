#ifndef _APPLICATION_LOG_H_
#define _APPLICATION_LOG_H_

#include "struct.h"

namespace logger {

class ApplicationLog {
public:
	ApplicationLog();
	~ApplicationLog();

	bool getParamFileInfo(const std::string& json_param, std::string& content);
	bool addLogFile(const std::string& json_param, std::string& conten);
	const std::string& getFileContent();

private:
	bool loadDefaultFileInfo();

private:
	// FileCache ...
	std::map<std::string, NetFileInfo> curr_file_info_;
};


};

#endif // !_APPLICATION_LOG_H_