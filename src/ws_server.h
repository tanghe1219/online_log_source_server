#ifndef _WS_SERVER_H_
#define _WS_SERVER_H_

#include "websocket/ws.h"
#include <memory>
#include "muduo/net/TcpServer.h"
#include <map>

/*
    websocket server
*/

using namespace muduo;
using namespace muduo::net;

namespace logger {

    typedef std::map<std::string, std::shared_ptr<ws_conn_t> > WebsocketMap;
	typedef std::function<void(ws_conn_t*, std::string&)> WsReqCb;
	typedef std::function<void(ws_conn_t*)> WsCloseCb;

class WsServer {
public:
   
    WsServer(EventLoop* loop, const InetAddress& listenAddr,
            const string& name,
            TcpServer::Option option = TcpServer::kNoReusePort);

    ~WsServer();

    EventLoop* getLoop() const { return server_.getLoop(); }

    void setWsReqCb(const WsReqCb& cb) {
        ws_req_cb_ = cb;
    }

    void setWSCloseCb(const WsCloseCb& cb) {
        ws_close_cb_ = cb;
    }

    void setThreadNum(int numThreads) {
        server_.setThreadNum(numThreads);
    }

    void start();

    static bool Send(void *arg, std::string data);

private:
    
    void onConnection(const TcpConnectionPtr& conn);
    void onRequest(void *arg, std::string data);
    void onClose(void *arg, std::string data);

    TcpServer server_;
    // WebsocketMap websockets_map_; 
    WsReqCb ws_req_cb_;
    WsCloseCb ws_close_cb_;
};

};

#endif // _WS_SERVER_H_