#include "muduo/net/http/HttpServer.h"
#include "muduo/net/http/HttpRequest.h"
#include "muduo/net/http/HttpResponse.h"
#include "muduo/net/EventLoop.h"
#include "muduo/base/Logging.h"
#include "muduo/base/Singleton.h"
#include <iostream>
#include <map>
#include "interface.h"
#include "res_reportor.h"
#include "system_log.h"
#include "application_log.h"
#include "lru_cache.h"
#include "./file_include/JsonDatabase.h"
#include "websocket/ws.h"
#include "ws_server.h"
#include "tailor.h"

using namespace muduo;
using namespace muduo::net;

FILERESPONSE_INFO_LIST* logInfos = NULL;

extern char favicon[555];

void onHttpRequest(const HttpRequest& req, HttpResponse* resp)
{
	// std::cout << "Headers " << req.methodString() << " " << req.path() << std::endl;

	if (req.path() == logger::SYSTEM_RES_URL.c_str()) {
		std::string body;
		Singleton<logger::ResReporter>::instance().getResource(body);

		resp->setStatusCode(HttpResponse::k200Ok);
		resp->setStatusMessage("OK");
		resp->setContentType("application/json");
		resp->addHeader("Access-Control-Allow-Origin", "*");
		resp->addHeader("Access-Control-Allow-Credentials", "true");
		resp->addHeader("Access-Control-Allow-Methods", "GET, POST");
		resp->addHeader("Access-Control-Allow-Headers", "*");
		resp->addHeader("Server", "Muduo");
		resp->setBody(body);

	}
	else if (strncmp(req.path().c_str(), logger::SYSTEM_LOG_URL.c_str(), logger::SYSTEM_LOG_URL.size()) == 0) {
		std::string body;

		if (req.path() == logger::QUERY_SYS_LOG_URL.c_str()) {
			Singleton<logger::SystemLog>::instance().getLogList(req.getBody(), body);
		}
		else if (req.path() == logger::ADD_SYS_LOG_URL.c_str()) {
			Singleton<logger::SystemLog>::instance().addLogFile(req.getBody(), body);
		}
		else if (req.path() == logger::UPDATE_SYS_LOG_URL.c_str()) {
			Singleton<logger::SystemLog>::instance().updateLogFile(req.getBody(), body);
		}
		else if (req.path() == logger::REMOVE_SYS_LOG_URL.c_str()) {
			Singleton<logger::SystemLog>::instance().removeLogFile(req.getBody(), body);
		}
		else {
			Singleton<logger::SystemLog>::instance().getParamFileInfo(req.getBody(), body);
		}

		resp->setStatusCode(HttpResponse::k200Ok);
		resp->setStatusMessage("OK");
		resp->setContentType("application/json");
		resp->addHeader("Server", "Muduo");
		resp->addHeader("Access-Control-Allow-Origin", "*");
		resp->addHeader("Access-Control-Allow-Credentials", "true");
		resp->addHeader("Access-Control-Allow-Methods", "GET, POST");
		resp->addHeader("Access-Control-Allow-Headers", "*");
		resp->setBody(body);

	}
	else if (req.path() == logger::APP_LOG_URL.c_str()) {

		char* response = NULL;
		JSONREQUEST_INFO* request = NULL;
		//应用日志信息结构体
		FILERESPONSE_INFO_LIST* appInfos = NULL;

		//const char *body = "{\"file-name\":\"mysql\",\"file-seg-start\":1,\"file-seg-end\":10}";

		//printf("parse body before\n");
		request = parseRequstData(req.getBody().c_str());
		//printf("request->filename==%s,seg-start==%d, seg-end==%d\n", request->appname, atoi(request->seg_start), atoi(request->seg_end));
		//printf("parse body after\n");

		//printf("scan app log dir before\n");
		scanAppLogFilepath(request->filepath, request->appname, request->seg_start, request->seg_end, &appInfos);
		//printf("scan app log dir after\n");
		
		// printf("package response data before\n");
		if (appInfos)
			response = packageResponseData(appInfos);
		else
			response = responseErrorMsg("3", "应用日志文件找不到");
		// printf("response data[%s]\n", response);
		// printf("package response data after\n");

		//释放资源
		cleanupResource(appInfos);

		resp->setStatusCode(HttpResponse::k200Ok);
		resp->setStatusMessage("OK");
		resp->setContentType("application/json");
		resp->addHeader("Server", "Muduo");
		resp->addHeader("Access-Control-Allow-Origin", "*");
		resp->addHeader("Access-Control-Allow-Credentials", "true");
		resp->addHeader("Access-Control-Allow-Methods", "GET, POST");
		resp->addHeader("Access-Control-Allow-Headers", "*");
		resp->setBody(response);

		cleanupRequest(request);
		free(request);
		free(response);
	}
	else if (req.path() == logger::QUERY_APP_LOG_URL.c_str())//应用日志查询
	{
		static int status = 1;
		char* response = NULL;
		JSONREQUEST_INFO* request = NULL;

		//printf("request body ==%s\n", req.getBody().c_str());
		//printf("parse query  body before\n");
		request = parseRequstData(req.getBody().c_str());
		//printf("topdir==%s\n", request->topdir);
		//printf("parse query body after\n");

		if (status == 1)
		{
			//printf("scan app name on system before\n");
			scanAppnameOnSystem(request->topdir, &logInfos);
			//printf("scan app name on system after\n");
			status = 0;
		}
		if (logInfos != NULL)
			response = queryAppInfos(logInfos);
		else
			response = responseErrorMsg("3", "应用日志文件找不到");
		resp->setStatusCode(HttpResponse::k200Ok);
		resp->setStatusMessage("OK");
		resp->setContentType("application/json");
		resp->addHeader("Server", "Muduo");
		resp->addHeader("Access-Control-Allow-Origin", "*");
		resp->addHeader("Access-Control-Allow-Credentials", "true");
		resp->addHeader("Access-Control-Allow-Methods", "GET, POST");
		resp->addHeader("Access-Control-Allow-Headers", "*");
		resp->setBody(response);

		cleanupRequest(request);
		free(request);
		free(response);
	}
	else if (req.path() == logger::ADD_APP_LOG_URL.c_str())//增加自定义应用 name  filepath
	{
		char* response = NULL;
		JSONREQUEST_INFO* request = NULL;

		//printf("request body ==%s\n", req.getBody().c_str());
		//printf("parse add  body before\n");
		request = parseRequstData(req.getBody().c_str());
		//printf("filename==%s,filepath=%s\n", request->appname, request->filepath);
		//printf("parse add body after\n");
		if (addAppInfoToList(request->appname, request->filepath, &logInfos) == 1)
		{
			response = responseErrorMsg("0", "增加成功");
		}
		else
		{
			response = responseErrorMsg("2", "增加失败");
		}
		//printf("response==%s\n", response);
		resp->setStatusCode(HttpResponse::k200Ok);
		resp->setStatusMessage("OK");
		resp->setContentType("application/json");
		resp->addHeader("Server", "Muduo");
		resp->addHeader("Access-Control-Allow-Origin", "*");
		resp->addHeader("Access-Control-Allow-Credentials", "true");
		resp->addHeader("Access-Control-Allow-Methods", "GET, POST");
		resp->addHeader("Access-Control-Allow-Headers", "*");
		resp->setBody(response);

		cleanupRequest(request);
		free(request);
		free(response);
	}
	else if (req.path() == logger::REMOVE_APP_LOG_URL.c_str()) //移除应用日志从应用日志内存中
	{
		char* response = NULL;
		JSONREQUEST_INFO* request = NULL;

		//printf("request body ==%s\n", req.getBody().c_str());
		//printf("parse remove  body before\n");
		request = parseRequstData(req.getBody().c_str());
		//printf("filename==%s,filepath=%s\n", request->appname, request->filepath);
		//printf("parse remove body after\n");
		if (deleteAppInfoFromList(request->appname, request->filepath, &logInfos) == 1)
		{
			response = responseErrorMsg("0", "移除成功");
		}
		else
		{
			response = responseErrorMsg("2", "移除失败");
		}
		//printf("response==%s\n", response);
		resp->setStatusCode(HttpResponse::k200Ok);
		resp->setStatusMessage("OK");
		resp->setContentType("application/json");
		resp->addHeader("Server", "Muduo");
		resp->addHeader("Access-Control-Allow-Origin", "*");
		resp->addHeader("Access-Control-Allow-Credentials", "true");
		resp->addHeader("Access-Control-Allow-Methods", "GET, POST");
		resp->addHeader("Access-Control-Allow-Headers", "*");
		resp->setBody(response);

		cleanupRequest(request);
		free(request);
		free(response);
	}
	else if (req.path() == logger::UPDATE_APP_LOG_URL.c_str()) //更新/修改应用日志文件路径
	{
		char* response = NULL;
		JSONREQUEST_INFO* request = NULL;

		//printf("request body ==%s\n", req.getBody().c_str());
		//printf("parse update  body before\n");
		request = parseRequstData(req.getBody().c_str());
		//printf("filename==%s,filepath=%s\n", request->appname, request->filepath);
		//printf("parse update body after\n");
		if (updateAppInfoFromList(request->appname, request->filepath, logInfos) == 1)
		{
			response = responseErrorMsg("0", "更新成功");
		}
		else
		{
			response = responseErrorMsg("2", "更新失败");
		}
		//printf("response==%s\n", response);
		resp->setStatusCode(HttpResponse::k200Ok);
		resp->setStatusMessage("OK");
		resp->setContentType("application/json");
		resp->addHeader("Server", "Muduo");
		resp->addHeader("Access-Control-Allow-Origin", "*");
		resp->addHeader("Access-Control-Allow-Credentials", "true");
		resp->addHeader("Access-Control-Allow-Methods", "GET, POST");
		resp->addHeader("Access-Control-Allow-Headers", "*");
		resp->setBody(response);

		cleanupRequest(request);
		free(request);
		free(response);
	}
	else {
		resp->setStatusCode(HttpResponse::k404NotFound);
		resp->setStatusMessage("Not Found");
		resp->setCloseConnection(true);
	}
}

void onWSRequest(logger::ws_conn_t* ws_conn, std::string& ws_buff) {
	if (ws_conn->request.uri == logger::RT_LOG_URL) {
		Singleton<logger::Tailor>::instance().startTrace(ws_conn);
	}
}

void onWSClose(logger::ws_conn_t* ws_conn) {
	if (ws_conn->request.uri == logger::RT_LOG_URL) {
		Singleton<logger::Tailor>::instance().stopTrace(ws_conn);
	}
}

int main(int argc, char* argv[])
{
	int numThreads = 0;
	if (argc > 1)
	{
		Logger::setLogLevel(Logger::WARN);
		numThreads = atoi(argv[1]);
	}

	Singleton<logger::ResReporter>::instance();
	Singleton<logger::SystemLog>::instance();
	Singleton<logger::ApplicationLog>::instance();
	Singleton<logger::CLRUCache>::instance();
	Singleton<logger::Tailor>::instance();

	EventLoop loop;
	HttpServer server(&loop, InetAddress(8000), "LogResourceSvr");
	server.setHttpCallback(onHttpRequest);
	server.setThreadNum(numThreads);
	server.start();

	logger::WsServer ws_server(&loop, InetAddress(8001), "WebsocketSvr");
	ws_server.setWsReqCb(onWSRequest);
	ws_server.setWSCloseCb(onWSClose);
	ws_server.setThreadNum(numThreads);
	ws_server.start();
	Singleton<logger::Tailor>::instance().setLoop(&loop);
	Singleton<logger::Tailor>::instance().start();

	loop.loop();
}

char favicon[555] = {
	'\x89', 'P', 'N', 'G', '\xD', '\xA', '\x1A', '\xA',
	'\x0', '\x0', '\x0', '\xD', 'I', 'H', 'D', 'R',
	'\x0', '\x0', '\x0', '\x10', '\x0', '\x0', '\x0', '\x10',
	'\x8', '\x6', '\x0', '\x0', '\x0', '\x1F', '\xF3', '\xFF',
	'a', '\x0', '\x0', '\x0', '\x19', 't', 'E', 'X',
	't', 'S', 'o', 'f', 't', 'w', 'a', 'r',
	'e', '\x0', 'A', 'd', 'o', 'b', 'e', '\x20',
	'I', 'm', 'a', 'g', 'e', 'R', 'e', 'a',
	'd', 'y', 'q', '\xC9', 'e', '\x3C', '\x0', '\x0',
	'\x1', '\xCD', 'I', 'D', 'A', 'T', 'x', '\xDA',
	'\x94', '\x93', '9', 'H', '\x3', 'A', '\x14', '\x86',
	'\xFF', '\x5D', 'b', '\xA7', '\x4', 'R', '\xC4', 'm',
	'\x22', '\x1E', '\xA0', 'F', '\x24', '\x8', '\x16', '\x16',
	'v', '\xA', '6', '\xBA', 'J', '\x9A', '\x80', '\x8',
	'A', '\xB4', 'q', '\x85', 'X', '\x89', 'G', '\xB0',
	'I', '\xA9', 'Q', '\x24', '\xCD', '\xA6', '\x8', '\xA4',
	'H', 'c', '\x91', 'B', '\xB', '\xAF', 'V', '\xC1',
	'F', '\xB4', '\x15', '\xCF', '\x22', 'X', '\x98', '\xB',
	'T', 'H', '\x8A', 'd', '\x93', '\x8D', '\xFB', 'F',
	'g', '\xC9', '\x1A', '\x14', '\x7D', '\xF0', 'f', 'v',
	'f', '\xDF', '\x7C', '\xEF', '\xE7', 'g', 'F', '\xA8',
	'\xD5', 'j', 'H', '\x24', '\x12', '\x2A', '\x0', '\x5',
	'\xBF', 'G', '\xD4', '\xEF', '\xF7', '\x2F', '6', '\xEC',
	'\x12', '\x20', '\x1E', '\x8F', '\xD7', '\xAA', '\xD5', '\xEA',
	'\xAF', 'I', '5', 'F', '\xAA', 'T', '\x5F', '\x9F',
	'\x22', 'A', '\x2A', '\x95', '\xA', '\x83', '\xE5', 'r',
	'9', 'd', '\xB3', 'Y', '\x96', '\x99', 'L', '\x6',
	'\xE9', 't', '\x9A', '\x25', '\x85', '\x2C', '\xCB', 'T',
	'\xA7', '\xC4', 'b', '1', '\xB5', '\x5E', '\x0', '\x3',
	'h', '\x9A', '\xC6', '\x16', '\x82', '\x20', 'X', 'R',
	'\x14', 'E', '6', 'S', '\x94', '\xCB', 'e', 'x',
	'\xBD', '\x5E', '\xAA', 'U', 'T', '\x23', 'L', '\xC0',
	'\xE0', '\xE2', '\xC1', '\x8F', '\x0', '\x9E', '\xBC', '\x9',
	'A', '\x7C', '\x3E', '\x1F', '\x83', 'D', '\x22', '\x11',
	'\xD5', 'T', '\x40', '\x3F', '8', '\x80', 'w', '\xE5',
	'3', '\x7', '\xB8', '\x5C', '\x2E', 'H', '\x92', '\x4',
	'\x87', '\xC3', '\x81', '\x40', '\x20', '\x40', 'g', '\x98',
	'\xE9', '6', '\x1A', '\xA6', 'g', '\x15', '\x4', '\xE3',
	'\xD7', '\xC8', '\xBD', '\x15', '\xE1', 'i', '\xB7', 'C',
	'\xAB', '\xEA', 'x', '\x2F', 'j', 'X', '\x92', '\xBB',
	'\x18', '\x20', '\x9F', '\xCF', '3', '\xC3', '\xB8', '\xE9',
	'N', '\xA7', '\xD3', 'l', 'J', '\x0', 'i', '6',
	'\x7C', '\x8E', '\xE1', '\xFE', 'V', '\x84', '\xE7', '\x3C',
	'\x9F', 'r', '\x2B', '\x3A', 'B', '\x7B', '7', 'f',
	'w', '\xAE', '\x8E', '\xE', '\xF3', '\xBD', 'R', '\xA9',
	'd', '\x2', 'B', '\xAF', '\x85', '2', 'f', 'F',
	'\xBA', '\xC', '\xD9', '\x9F', '\x1D', '\x9A', 'l', '\x22',
	'\xE6', '\xC7', '\x3A', '\x2C', '\x80', '\xEF', '\xC1', '\x15',
	'\x90', '\x7', '\x93', '\xA2', '\x28', '\xA0', 'S', 'j',
	'\xB1', '\xB8', '\xDF', '\x29', '5', 'C', '\xE', '\x3F',
	'X', '\xFC', '\x98', '\xDA', 'y', 'j', 'P', '\x40',
	'\x0', '\x87', '\xAE', '\x1B', '\x17', 'B', '\xB4', '\x3A',
	'\x3F', '\xBE', 'y', '\xC7', '\xA', '\x26', '\xB6', '\xEE',
	'\xD9', '\x9A', '\x60', '\x14', '\x93', '\xDB', '\x8F', '\xD',
	'\xA', '\x2E', '\xE9', '\x23', '\x95', '\x29', 'X', '\x0',
	'\x27', '\xEB', 'n', 'V', 'p', '\xBC', '\xD6', '\xCB',
	'\xD6', 'G', '\xAB', '\x3D', 'l', '\x7D', '\xB8', '\xD2',
	'\xDD', '\xA0', '\x60', '\x83', '\xBA', '\xEF', '\x5F', '\xA4',
	'\xEA', '\xCC', '\x2', 'N', '\xAE', '\x5E', 'p', '\x1A',
	'\xEC', '\xB3', '\x40', '9', '\xAC', '\xFE', '\xF2', '\x91',
	'\x89', 'g', '\x91', '\x85', '\x21', '\xA8', '\x87', '\xB7',
	'X', '\x7E', '\x7E', '\x85', '\xBB', '\xCD', 'N', 'N',
	'b', 't', '\x40', '\xFA', '\x93', '\x89', '\xEC', '\x1E',
	'\xEC', '\x86', '\x2', 'H', '\x26', '\x93', '\xD0', 'u',
	'\x1D', '\x7F', '\x9', '2', '\x95', '\xBF', '\x1F', '\xDB',
	'\xD7', 'c', '\x8A', '\x1A', '\xF7', '\x5C', '\xC1', '\xFF',
	'\x22', 'J', '\xC3', '\x87', '\x0', '\x3', '\x0', 'K',
	'\xBB', '\xF8', '\xD6', '\x2A', 'v', '\x98', 'I', '\x0',
	'\x0', '\x0', '\x0', 'I', 'E', 'N', 'D', '\xAE',
	'B', '\x60', '\x82',
};
