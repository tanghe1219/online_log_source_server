#include "ws_server.h"
#include <iostream>
#include "muduo/net/http/HttpServer.h"

using namespace muduo;
using namespace muduo::net;

namespace logger {

WsServer::WsServer(EventLoop* loop,
                    const InetAddress& listenAddr,
                    const string& name,
                    TcpServer::Option option)
  : server_(loop, listenAddr, name, option) {
    server_.setConnectionCallback(
        std::bind(&WsServer::onConnection, this, _1));
    // server_.setMessageCallback(
    //     std::bind(&WsServer::onMessage, this, _1, _2, _3));
    ws_req_cb_ = nullptr;
    ws_close_cb_ = nullptr;
}

WsServer::~WsServer() {
}

void WsServer::start()
{
    std::cout << "Websocket[" << server_.name()
        << "] starts listenning on " << server_.ipPort() << std::endl;
    server_.start();
}

void WsServer::onConnection(const TcpConnectionPtr& conn)
{
    if (conn->connected())
    {
        conn->setContext(ws_conn_t());
        ws_conn_t* context = boost::any_cast<ws_conn_t>(conn->getMutableContext());
        ws_conn_setcb(context, FRAME_RECV, std::bind(&WsServer::onRequest, this, _1, _2), context);
        ws_conn_setcb(context, CLOSE, std::bind(&WsServer::onClose, this, _1, _2), context);
        context->conn = conn;
        
        ws_serve_start(context);
    }
    else if (conn->disconnected()) {
        ws_conn_t* context = boost::any_cast<ws_conn_t>(conn->getMutableContext());
        onClose(context, "");
    }
}

void WsServer::onClose(void *arg, std::string data) {
    ws_conn_t *context = (ws_conn_t *)arg;
    context->conn.reset();
    // ws_conn_free(context);
    if (ws_close_cb_) {
        ws_close_cb_(context);
    }
}

void WsServer::onRequest(void *arg, std::string data) {
    ws_conn_t *context = (ws_conn_t *)arg;

    if (context->frame.fin != 1) {
        return;
    }
    
    // preserve
    std::string msg ;
    if (context->frame.payload_len > 0) {
        msg = context->frame.payload_data;
    }

    // std::cout << "recv msg: " << data.size() << "parse data: " << msg.size() << std::endl;

    std::string payload;

    if (ws_req_cb_) {
        ws_req_cb_(context, payload);
    }
    
    if (payload.empty()) { // 如果只是触发定时回调，这里就是空，不需要发送
        return;
    }

    frame_buffer_t fb;
    frame_buffer_new(1, 1, payload.size(), payload.c_str(), fb);
	if (send_a_frame(context, &fb) != 0) {
		printf("send message failed\n");
	}
}

bool WsServer::Send(void *arg, std::string data) {
    ws_conn_t *context = (ws_conn_t *)arg;
    if (!context || !context->conn.get()) {
        return false;
    }
    
    frame_buffer_t fb;
    frame_buffer_new(1, 1, data.size(), data.c_str(), fb);
	if (send_a_frame(context, &fb) != 0) {
		printf("send message failed\n");
	}

    return true;
}

};