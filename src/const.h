#ifndef _CONST_H_
#define _CONST_H_

namespace logger {

static std::map<std::string, std::string> DEFAULT_SYSTEM_LOG_PATH = {
    { "syslog", "/var/log/messages" },
    { "dmesg", "/usr/bin/dmesg" },
    { "mail_log", "/var/log/maillog" },
    { "cron_log", "/var/log/cron" },
    { "last_log", "/usr/bin/lastlog" },
    { "secure_log", "/var/log/secure" },
};

static const std::string& VAR_LOG_CONST = "/var/log";
static const std::string& USR_BIN_CONST = "/usr/bin";

static std::map<std::string, std::string> DEFAULT_APP_LOG_PATH = {
    { "mysql,", "" },
    { "nginx", "" },
    { "tomcat", "" },
    { "apache", "" },
    { "mongo", "" },
    { "redis", "" },
};

// file type const
static const std::string ASCII_TEXT_TYPE = " ASCII text";
static const std::string UTF8_TEXT_TYPE = " UTF-8 Unicode text";
static const std::string DIRECTORY_TYPE = " directory";
static const std::string ELF_TYPE = " ELF";
static const std::string JPEG_TYPE = " JPEG image data";

// error code & des
static const std::string UNKNOW_FILE_ERR_STR = "找不到对应的文件名";
static const std::string SERVER_INTER_ERR_STR = "服务器内部错误";
static const std::string NEED_MUST_PARAM_ERR_STR = "需要必须的参数";
static const std::string FILE_EXIST_ERR_STR = "文件已经存在";
static const std::string FILE_NO_EXIST_ERR_STR = "文件已经存在";
static const std::string QUERY_FILE_FAILED_ERR_STR = "查找文件失败，请检查权限";
static const std::string FILE_LIST_EMPTY_ERR_STR = "文件列表为空";
static const std::string FILE_OPEN_FAILED_ERR_STR = "文件打开错误，请检查文件名";

static const std::string OPERATE_SUCCESS_STR = "操作成功";

// 默认读取tail -f之前的文件大小
static const int DEFAULT_TAIL_LINES = 10;
static const int REFRESH_DELAY = 5;

};

#endif // !_CONST_H_