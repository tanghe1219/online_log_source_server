
#include "lru_cache.h"

namespace logger {

const long CACAHE_TIME (300); // time 300 second, 5 minute

CLRUCache::CLRUCache(int limit)
{
	limit_ = limit;
	phead_ = nullptr;
	ptail_ = nullptr;
	cachetime_ = CACAHE_TIME;
	cachenumber_ = 0;
}


CLRUCache::~CLRUCache(void)
{

}


void CLRUCache::refreshNode(Node *pnode)
{
	if (pnode == phead_) {
		return;
	} 
	removeNode(pnode);
	setHead(pnode);
}

void CLRUCache::set(std::string key, void* value, bool forever)
{
	if (forever) { // forever cache
		std::unordered_map<std::string, Node*>::iterator iter = hash_map.find(key);
		if (iter != hash_map.end()) {
			Node *pnode = iter->second;
			pnode->setValue(value);
		} else {
			Node *pnode = new Node(key, value, 0);
			hash_map[key] = pnode;		
		}
		return;
	}
	time_t  timeFlag = time(nullptr);
	std::unordered_map<std::string, Node*>::iterator iter = hash_map.find(key);
	if (iter != hash_map.end()) {
		Node *pnode = iter->second;
		pnode->setValue(value);
		pnode->setTime(timeFlag);
		refreshNode(pnode);
	} else {
		if (cachenumber_ >= limit_)
		{
			Node* tmp = ptail_;
			std::string tailKey = ptail_->getKey(); 
			removeNode(ptail_);
			delete tmp;
			tmp = nullptr;
			hash_map.erase(tailKey);
			cachenumber_--;
		}
		Node *pnode = new Node(key, value, timeFlag);
		setHead(pnode);
		hash_map[key] = pnode;		
		cachenumber_++;		
	}
}

void CLRUCache::removeKey(std::string key)
{
	std::unordered_map<std::string, Node*>::iterator iter = hash_map.find(key);
	if (iter != hash_map.end()) {
		Node *pnode = iter->second;
		if (pnode->getTime() > 0) {
			cachenumber_--;
			removeNode(pnode);			
		}
		// delete data
		delete pnode;
		pnode = nullptr;
		hash_map.erase(key);
	}
}

void* CLRUCache::get(std::string key)
{
	refreshCache();
	std::unordered_map<std::string, Node*>::iterator iter = hash_map.find(key);
	if (iter != hash_map.end()) {
		Node *pnode = iter->second;
		if (pnode->getTime() > 0) {
			refreshNode(pnode);
			time_t  timeFlag = time(nullptr);
			pnode->setTime(timeFlag);
		}
		return pnode->getValue();
	} else {
		return nullptr;
	}
}
	
bool CLRUCache::getForeverKey(std::string key)
{
	bool bForeverKey = false;
	std::unordered_map<std::string, Node*>::iterator iter = hash_map.find(key);
	if (iter != hash_map.end()) {
		Node *pnode = iter->second;
		if (pnode->getTime() == 0) {
			bForeverKey = true;
		}
	}
	return bForeverKey;
}

void CLRUCache::removeNode(Node *pnode)
{
	Node* tmp = pnode;
	if (nullptr != pnode->pre_ ) {
		pnode->pre_->next_ = pnode->next_;
	} else {
		phead_ = pnode->next_;
	}

	if (nullptr != pnode->next_) {
		pnode->next_->pre_ = pnode->pre_;
	} else {
		ptail_ = pnode->pre_;
		ptail_->next_ = nullptr;
	}
}

void CLRUCache::setHead(Node *pnode)
{
	pnode->next_ = phead_;
	pnode->pre_ = nullptr;
	if (nullptr != phead_) {
		phead_->pre_ = pnode;
	} 

	phead_ = pnode;
	if (nullptr == ptail_) {
		ptail_ = phead_;
	}
}

void CLRUCache::refreshCache()
{
	time_t curTime = time(nullptr);

	while(ptail_ != nullptr) {
		if ((long)difftime(curTime, ptail_->getTime()) >= cachetime_) {
			hash_map.erase(ptail_->getKey());
			Node *tmp = ptail_->pre_;
			delete ptail_;
			ptail_ = nullptr;
			ptail_ = tmp;
			ptail_->next_ = nullptr;
		} else {
			break;
		}
	}
}

int CLRUCache::getSize()
{
	return hash_map.size();
}

void CLRUCache::setCacheTime(int times)
{
	cachetime_ = times;
}

int CLRUCache::getCacheTime()
{
	return cachetime_;
}
void CLRUCache::showAll()
{
	std::unordered_map<std::string, Node*>::iterator iter = hash_map.begin();
	while (iter != hash_map.end()) {
		Node *tmp = iter->second;
		std::string key = iter->first;
		iter++;
		std::cout << tmp->getKey() << "\t" << tmp->getValue() <<  "\t" << tmp->getTime() << std::endl;
	}
}

};
