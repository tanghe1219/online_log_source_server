#ifndef _TAILOR_H_
#define _TAILOR_H_

#include <string>
#include <map>
#include "struct.h"
#include "muduo/net/TcpServer.h"
#include "muduo/net/TimerId.h"
#include "./tools/file_operator.h"
#include "websocket/connection.h"

/*
    tail -f 的实现模块
*/

using namespace muduo;
using namespace muduo::net;

namespace logger {

class WsServer;

class Tailor {
public:
	Tailor();
	~Tailor();

    // 定时器使用
    void setLoop(EventLoop *loop);
    void setSendHandler(void *context);

    void start();
    void stopTrace(logger::ws_conn_t* ws_conn);
    void startTrace(logger::ws_conn_t* ws_conn);

	bool getParamFileInfo(NetFileInfo& file_info, std::string& content, bool& empty);

private:
    void Refresh();

private:
    std::map<std::string, NetFileInfo> curr_file_info_;
    EventLoop *loop_;
    void* context_;
    TimerId timer_;
    int count_;
};

};

#endif // _TAILOR_H_