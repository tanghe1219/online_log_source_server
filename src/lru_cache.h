#pragma once
#include <iostream>
#include <unordered_map>
#include <string>
#include <time.h>

namespace logger {

class Node 
{
private:
	std::string key_;
	void*       value_;
	time_t      create_time_; 
		
public:
	class Node *next_;
	class Node *pre_;

	Node() : value_(nullptr), pre_(nullptr), next_(nullptr), create_time_(0){};

	Node(std::string key, void* value, time_t times) :
	key_(key), value_(value), create_time_(times), pre_(nullptr), next_(nullptr){
	}

	std::string getKey() {
		return key_;
	}

	void setKey(std::string key) {
		key_ = key;
	}

	void* getValue() {
		return value_;
	}

	void setValue(void* value) {
		value_ = value;
	}

	time_t getTime( ) {
		return create_time_;
	}

	void setTime(time_t times) {
		create_time_ = times;
	}

	std::string getNextKey() {
		return next_->getKey();
	}

	std::string getPreKey() {
		return pre_->getKey();
	}
};

class CLRUCache
{
public:
	CLRUCache(int limit = 100);
	~CLRUCache(void);

	void   set(std::string key, void* value, bool forever = false);
	void*  get(std::string key);
	bool   getForeverKey(std::string key);
	void   removeKey(std::string key);
	int    getSize();
	void   setCacheTime(int times);
	int    getCacheTime();
	void   showAll();

private:
	class  Node *phead_;
	class  Node *ptail_;
	int    limit_;          // cache number limit
	int    cachetime_;      // cache time limit
	int    cachenumber_;


	void   refreshNode(Node *pnode);
	void   removeNode(Node *pnode);
	void   setHead(Node *pnode);
	void   refreshCache();

	std::unordered_map<std::string, Node* > hash_map;
};

};
