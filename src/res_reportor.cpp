#include "res_reportor.h"
#include "json/json.h"
#include "interface.h"
#include "system_res/use_cpu.h"
#include "system_res/use_disk.h"
#include "system_res/use_mem.h"
#include "system_res/use_net.h"
#include "tools/string_operator.h"

namespace logger {

ResReporter::ResReporter() {

}

ResReporter::~ResReporter() {

}

bool ResReporter::getResource(std::string& content) {
	Json::Value root;
	Json::Value elem;

	double cpu_used = 0;
	logger::UseCpu::getCpuUsed(cpu_used);
	elem[CPU_ITEM.c_str()] = cpu_used;

	int mem_used = 0;
	logger::UseMem::getMemUsed(mem_used);
	elem[MEM_ITEM.c_str()] = mem_used;

	int disk_used = 0;
	logger::UseDisk::getDiskUsed(disk_used);
	elem[DISK_ITEM.c_str()] = disk_used;

	long int recv_rate = 0, send_rate = 0;
	logger::UseNet::getNetUsed(recv_rate, send_rate);
	elem[NET_RECV_ITEM.c_str()] = recv_rate;
	elem[NET_SEND_ITEM.c_str()] = send_rate;

	logger::TopValueMulMap cpu_top, mem_top;
	logger::UseCpu::getTopCpuAndMem(cpu_top, mem_top);

	std::string cpu_str;
	const int show_item_n = 5;
	logger::TopValueMulMap::reverse_iterator it_cpu = cpu_top.rbegin();
	for (int i = 0; i < show_item_n && it_cpu != cpu_top.rend(); ++it_cpu, i++) {
		cpu_str += StringOperator::DoubleToStr(it_cpu->first) + " " + it_cpu->second + "\n";
	}

	elem[CPU_TOP_ITEM.c_str()] = cpu_str;

	std::string mem_str;
	logger::TopValueMulMap::reverse_iterator it_mem = mem_top.rbegin();
	for (int i = 0; i < show_item_n && it_mem != mem_top.rend(); ++it_mem, i++) {
		mem_str += StringOperator::DoubleToStr(it_mem->first) + " " + it_mem->second + "\n";
	}
	elem[MEM_TOP_ITEM.c_str()] = mem_str;

	root[DATA_ITEM.c_str()] = elem;

	Json::StreamWriterBuilder builder;
	content = Json::writeString(builder, root);

	return true;
}

};
