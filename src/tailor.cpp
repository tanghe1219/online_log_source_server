#include "tailor.h"
#include "json/json.h"
#include "system_log.h"
#include "struct.h"
#include "const.h"
#include "interface.h"
#include "muduo/net/EventLoop.h"
#include "muduo/net/EventLoopThread.h"
#include "muduo/base/Thread.h"
#include "ws_server.h"
#include <iostream>
#include <memory>

using namespace muduo;
using namespace muduo::net;

namespace logger {

Tailor::Tailor() {
    count_ = 0;
}

Tailor::~Tailor() {
    loop_->cancel(timer_);
}

void Tailor::Refresh() {
    std::string content;
    
    std::map<std::string, NetFileInfo>::iterator it = curr_file_info_.begin();
    for (; it != curr_file_info_.end();) {
        bool empty = false;
		getParamFileInfo(it->second, content, empty);
		if (empty) {
            it++;
            continue;
		}

		if (!logger::WsServer::Send(it->second.arg, content)) {
            curr_file_info_.erase(it++);
            continue;
        }
        else {
            it++;
        }
    }
    
    timer_ = loop_->runAfter(REFRESH_DELAY, std::bind(&Tailor::Refresh, this));
}

void Tailor::setLoop(EventLoop *loop) {
    loop_ = loop;
}

void Tailor::setSendHandler(void *context) {
    context_ = context;
}

void Tailor::stopTrace(logger::ws_conn_t* ws_conn) {
	if (!ws_conn) {
		return;
	}

	std::map<std::string, NetFileInfo>::iterator it = curr_file_info_.begin();
    for (; it != curr_file_info_.end(); it++) {
        if (it->second.arg == &(*ws_conn)) {
            curr_file_info_.erase(it++);
            break;
        }
    }
}

void Tailor::start() {
    Refresh();
}

void Tailor::startTrace(logger::ws_conn_t* ws_conn) {
    if (!ws_conn) {
        return;
    }

    if (ws_conn->frame.payload_data.empty()) {
        return;
    }

	Json::CharReaderBuilder reader_build;
	std::unique_ptr<Json::CharReader> reader(reader_build.newCharReader());
	Json::Value root;
    Json::String errs;

	if (!reader->parse(ws_conn->frame.payload_data.c_str(), 
        ws_conn->frame.payload_data.c_str() + ws_conn->frame.payload_data.size(), 
        &root, &errs)) {
		return;
	}

	Json::Value file_name_val = root.get(FILE_NAME_ITEM.c_str(), "");

	if (file_name_val.asString().empty()) {
        return;
	}

	Json::Value full_file_path = root.get(FULL_FILE_PATH_ITEM.c_str(), "");
    if (full_file_path.asString().empty()) {
        return;
    }

    if (!ws_conn->conn.get()) {
        return;
    }

    std::string key = ws_conn->conn->peerAddress().toIpPort();
    std::map<std::string, NetFileInfo>::iterator iter;
    if ((iter = curr_file_info_.find(key)) == curr_file_info_.end()) {
        // 新建一个
        NetFileInfo net_file_info;
        net_file_info.file_name = file_name_val.asString();
        net_file_info.path_file_name = full_file_path.asString();
        net_file_info.arg = ws_conn;
        curr_file_info_[key] = net_file_info;
    }
    else {
        if (iter->second.file_name == file_name_val.asString()) {
			// 发个空过去表示响应
			logger::WsServer::Send(ws_conn, "{\"data\":\"==== START TRACING ====\"}");
            return;
        }

        iter->second.reset();
        iter->second.file_name = file_name_val.asString();
        iter->second.path_file_name = full_file_path.asString();
    }

    // 发个空过去表示响应
    logger::WsServer::Send(ws_conn, "{\"data\":\"==== START TRACING ====\"}");
}

bool Tailor::getParamFileInfo(NetFileInfo& file_info, std::string& content, bool& empty) {
	if (file_info.path_file_name.empty()) {
		return true;
	}
    
    Json::Value root_out;
    int errcode;
    std::string errmsg;
    std::string file_content;
    char s_data[16] = {0};

    do {
        
        FileOperator file_oper;
        size_t new_size = file_oper.getFileSize(file_info.path_file_name);
        if (new_size == file_info.file_size) { // 为空
            empty = true;
            return true;
        }

        file_info.file_size = new_size;

		if (!file_oper.open(file_info.path_file_name, BINARY_NO_BUFF)) {
			errcode = FILE_OPEN_FAILED_ERR;
			errmsg = FILE_OPEN_FAILED_ERR_STR;
			break;
		}

        if (!file_oper.readEndOfLine(file_info.last_line,
            file_info.last_pos,
            0,
            file_content)) {
            errcode = SERVER_INTER_ERR;
            errmsg = SERVER_INTER_ERR_STR;
            break;
        }

        if (file_content.empty()) {
            empty = true;
        } else {
            empty = false;
        }

        root_out[DATA_ITEM.c_str()] = file_content;
		Json::StreamWriterBuilder builder;
		content = Json::writeString(builder, root_out);
        return true;

    } while (0);
    
    memset(s_data, 0x0, sizeof(s_data));
    sprintf(s_data, "%d", errcode);
    root_out[ERROR_ITEM.c_str()] = s_data;
    root_out[ERRMSG_ITEM.c_str()] = errmsg;
	Json::StreamWriterBuilder builder;
	content = Json::writeString(builder, root_out);

    return false;
}

};