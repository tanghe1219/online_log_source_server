#ifndef _SYSTEM_LOG_H_
#define _SYSTEM_LOG_H_

#include "struct.h"

namespace logger {

class SystemLog	{
public:
	SystemLog();
	~SystemLog();

	bool getLogList(const std::string& json_param, std::string& content);
	bool getParamFileInfo(const std::string& json_param, std::string& content);
	bool addLogFile(const std::string& json_param, std::string& content);
	bool updateLogFile(const std::string& json_param, std::string& content);
	bool removeLogFile(const std::string& json_param, std::string& content);

private:
	bool loadDefaultFileInfo();

private:
	// FileCache ...
	std::map<std::string, NetFileInfo> curr_file_info_;
};


};

#endif // !_SYSTEM_LOG_H_