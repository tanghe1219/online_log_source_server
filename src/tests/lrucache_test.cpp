﻿#include <cstdio>
#include "lru_cache.h"
#include <iostream>
using namespace std;
#include "unistd.h"

class mydata
{
public:
	int len_;
	char* pdata_;

	mydata() : len_(0), pdata_(nullptr) {};

	mydata(int len, char* pdata) : len_(len), pdata_(pdata) {}
};

int main()
{
	
	logger::CLRUCache* mycache = new logger::CLRUCache();

	// test set/get struct data
	mydata* dat1 = new mydata();
	dat1->len_ = 6;
	dat1->pdata_ = (char*)"123456";
	mycache->set("dat1", dat1, true);

	mydata* dat2 = (mydata*)mycache->get("dat1");
	std::cout << dat2->len_ << "," << dat2->pdata_ << endl;

	// test set/get Forever cache data
	mycache->set("path1", (char*)"path1", true);
	mycache->set("path2", (char*)"path2", true);
	mycache->set("path3", (char*)"path3", true);
	mycache->set("path4", (char*)"path4", true);
	mycache->set("path5", (char*)"path5", true);
	mycache->set("path6", (char*)"path6", true);

	// test cache size
	std::cout << mycache->getSize() << endl;

	// test remove key, all key may be remove
	mycache->removeKey("path4");
	std::cout << mycache->getSize() << endl;


	mycache->set("001", (char*)"user 1 info");
	mycache->set("002", (char*)"user 2 info");
	mycache->set("003", (char*)"user 3 info");
	mycache->set("004", (char*)"user 4 info");
	mycache->set("005", (char*)"user 5 info");
	mycache->set("006", (char*)"user 6 info");

	int cachetime = mycache->getCacheTime();
	std::cout << "getCacheTime = " << cachetime << "second" << endl;

	bool bForeverkey1 = mycache->getForeverKey("path1");
	bool bForeverkey2 = mycache->getForeverKey("001");
	std::cout << "key = path1 is ForeverKey " << (bForeverkey1 ? "true" : "false") << endl;
	std::cout << "key = 001 is ForeverKey "   << (bForeverkey2 ? "true" : "false") << endl;

	int szie = mycache->getSize();
	std::cout << "cache size = " << szie << endl;


	string str2 = (char*)mycache->get("002");
	std::cout << str2 << endl;
	mycache->set("002", (char*)"user 2 info update 1");
	str2 = (char*)mycache->get("002");
	std::cout << str2 << endl;

	sleep(3);
	std::cout << mycache->getSize() << endl;

	mycache->removeKey("001");
	std::cout << mycache->getSize() << endl;

	mycache->set("007", (char*)"user 1 info");
	mycache->set("008", (char*)"user 2 info");
	//sleep(3);
	mycache->set("009", (char*)"user 3 info");
	mycache->set("0010", (char*)"user 4 info");
	//sleep(3);
	mycache->set("0011", (char*)"user 5 info");
	mycache->set("0012", (char*)"user 6 info");

	for (int i = 0; i < 10; i++)
	{
		string strdata;
		char sdata[8] = { 0 };
		sprintf(sdata, "%d", i);
		mycache->set(sdata, sdata);
		sleep(1);
	}
	mycache->get("111");
	std::cout << mycache->getSize() << endl;
	mycache->showAll();
	std::cout << mycache->getSize() << endl;
	system("pause");
	//getchar();

    return 0;
}