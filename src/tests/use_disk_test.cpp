#include <iostream>
#include "../struct.h"
#include <unistd.h>
#include <stdlib.h>
#include <string>
#include "../system_res/use_disk.h"

using namespace std;
using namespace logger;

int main() {

	int cnt = 0;
	do {
		int used_rate = 0;
		logger::UseDisk::getDiskUsed(used_rate);

		cout << "used_rate: " << used_rate << endl;

		sleep(1);
	} while (cnt++ < 10);

	return 0;
}
