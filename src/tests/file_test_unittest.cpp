#include <iostream>
#include "../tools/file_operator.h"
#include "../struct.h"
#include <unistd.h>
#include <stdlib.h>
#include <string>

using namespace std;
using namespace logger;

int main() {
    FileOperator file_oper;
    file_oper.open("/var/log/messages", TEXT_WITH_BUFF);

    int cnt = 0;
    size_t last_line = 0;
    size_t last_pos = 0;
    do {
        size_t lines = 0;
        file_oper.getTotalLines(lines);

        cout << "get total_lines: " << lines << endl;

        std::string content;
        file_oper.readEndOfLine(last_line, last_pos, 10, content);

        cout << "last 2 lines: " << content << endl;

        sleep(1);
    } while (cnt ++ < 10);

    return 0;
}
