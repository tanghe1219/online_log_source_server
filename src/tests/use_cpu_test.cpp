#include <iostream>
#include "../struct.h"
#include <unistd.h>
#include <stdlib.h>
#include <string>
#include "../system_res/use_cpu.h"

using namespace std;
using namespace logger;

int main() {

	int cnt = 0;
	do {
		double usage = 0;
		logger::UseCpu::getCpuUsed(usage);

		logger::TopValueMulMap cpu_top, mem_top;
		logger::UseCpu::getTopCpuAndMem(cpu_top, mem_top);

		cout << "usage: " << usage << ", size: " << cpu_top.size() << endl;

		sleep(1);
	} while (cnt++ < 10);

	return 0;
}
