add_executable(file_test_unittest file_test_unittest.cpp) 
target_link_libraries(file_test_unittest muduo_base muduo_net log_resource)

add_executable(file_lseek_unittest file_lseek_unittest.cpp) 
target_link_libraries(file_lseek_unittest muduo_base muduo_net log_resource)

add_executable(use_net_test use_net_test.cpp) 
target_link_libraries(use_net_test muduo_base muduo_net log_resource)

add_executable(use_mem_test use_mem_test.cpp) 
target_link_libraries(use_mem_test muduo_base muduo_net log_resource)

add_executable(use_disk_test use_disk_test.cpp) 
target_link_libraries(use_disk_test muduo_base muduo_net log_resource)

add_executable(use_cpu_test use_cpu_test.cpp) 
target_link_libraries(use_cpu_test muduo_base muduo_net log_resource)

