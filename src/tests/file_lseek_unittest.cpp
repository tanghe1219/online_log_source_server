#include <iostream>
#include "../tools/file_operator.h"
#include "../struct.h"
#include <unistd.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <string>
#include <errno.h>

using namespace std;
using namespace logger;

int main() {
    FileOperator file_oper;
    file_oper.open("/var/log/messages", BINARY_NO_BUFF);

    int cnt = 0;
    size_t last_line = 0;
    size_t last_pos = 0;

    // int fd = ::open("/var/log/messages", O_RDONLY);
    // char block[4096] = {0};
    do {
        size_t lines = 0;
        // file_oper.getTotalLines(lines);
        // cout << "get total_lines: " << lines << endl;

        std::string content;
        file_oper.readEndOfLine(last_line, last_pos, 0, content);

        cout << "last N lines: " << std::endl << content << endl;

        // lseek(fd, 22802, SEEK_SET);
        // size_t read_sz = ::read(fd, block, 4096);
        // if (read_sz < 0) {
        //     cout << "error occur" << errno << endl;
        //     return -1;
        // }

        // if (read_sz == 0) {
        //     cout << "read end of file" << endl;
        //     break;
        // }

        // cout << "read content: " << endl << block << endl;
        // break;

        sleep(5);
    } while (cnt ++ < 100);

    return 0;
}
