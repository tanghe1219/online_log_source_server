#include <iostream>
#include "../struct.h"
#include <unistd.h>
#include <stdlib.h>
#include <string>
#include "../system_res/use_mem.h"

using namespace std;
using namespace logger;

int main() {

	int cnt = 0;
	do {
		int usage = 0;
		logger::UseMem::getMemUsed(usage);

		cout << "usage: " << usage << endl;

		sleep(1);
	} while (cnt++ < 10);

	return 0;
}
