#include <iostream>
#include "../struct.h"
#include <unistd.h>
#include <stdlib.h>
#include <string>
#include "../system_res/use_net.h"

using namespace std;
using namespace logger;

int main() {

	int cnt = 0;
	do {
		long int recv_rate = 0, send_rate = 0;
		logger::UseNet::getNetUsed(recv_rate, send_rate);
		
		cout << "recv: " << recv_rate << ", send: " << send_rate << endl;

		sleep(1);
	} while (cnt++ < 10);

	return 0;
}
