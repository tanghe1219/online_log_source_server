#ifndef FILEDATABASE_H_INCLUDE
#define FILEDATABASE_H_INCLUDE

typedef struct
{
    char *filepath;//日志路径
    char *filename;//日志文件名
    char *classname;//静态日志/动态日志
    char *data; //前端请求的日志内容
    char *type;//0 系统日志 1应用日志
    char *appname;//应用日志名
    char *apptype;//区分默认日志和自定义日志
    char *fileSegStart;//前端获取日志内容起始行
    char *fileSegEnd; //前端获取日志内容结束行
    int  currentFileSize;//当前文件大小
    int  beforeFileSize;//记录请求前的文件大小
}FILERESPONSE_INFO;

typedef struct
{
   void *next;
   FILERESPONSE_INFO *info;
}FILERESPONSE_INFO_LIST;
#ifdef __cplusplus
extern "C"{
#endif

int getAppType(char *appname);

int get_file_line(char *result,char *fileName,int lineNumber);
//获取应用日志内容
void getAppLogContents();
//扫描系统日志目录下应用日志路径信息
void scanAppLogFilepath(char *filepath, char *appname , char *seg_start, char *seg_end, FILERESPONSE_INFO_LIST **logInfos);
//释放资源
void cleanupResource(FILERESPONSE_INFO_LIST *infos);

void scanAppnameOnSystem(char *dir, FILERESPONSE_INFO_LIST **logInfos);

int updateAppInfoFromList(char *filename, char *filepath, FILERESPONSE_INFO_LIST *logInfos);

//从应用日志信息链表中删除节点
int deleteAppInfoFromList(char *filename, char *filepath, FILERESPONSE_INFO_LIST **logInfos);

//查询当前系统常用应用日志
int addAppInfoToList(char *appname , char *filepath, FILERESPONSE_INFO_LIST **logInfos);

//void queryAppInfos(FILERESPONSE_INFO_LIST *infos);

#ifdef __cplusplus
}
#endif
#endif
