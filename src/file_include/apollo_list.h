#ifndef __APOLLO_LIST_H
#define __APOLLO_LIST_H

#include "apollo_systype.h"

typedef struct
{
    PVOID     next;
    PVOID     info;
}APOLLO_LIST_HEAD;

typedef VOID ( * APOLLO_DELETE_NODE_CALLBACK )( PVOID node );
typedef INT32_T ( * APOLLO_LIST_ORDERBY_CALLBACK )( PVOID node1, PVOID node2 );
typedef RTSTATUS ( * APOLLO_LIST_FILTER_CALLBACK )( PVOID node1, PVOID node2 );

VOID  apollo_delete_node( PVOID * node, APOLLO_DELETE_NODE_CALLBACK cb );
PVOID apollo_create_node( INT32_T length );

VOID  apollo_delete_list_node( APOLLO_LIST_HEAD ** node, APOLLO_DELETE_NODE_CALLBACK cb );
VOID  apollo_delete_list( APOLLO_LIST_HEAD ** list, APOLLO_DELETE_NODE_CALLBACK cb );
APOLLO_LIST_HEAD * apollo_create_list_node( PVOID info );
VOID  apollo_add_node_to_list( APOLLO_LIST_HEAD ** list, APOLLO_LIST_HEAD * node, APOLLO_LIST_FILTER_CALLBACK filter, APOLLO_DELETE_NODE_CALLBACK delnode, APOLLO_LIST_ORDERBY_CALLBACK orderby );
VOID  apollo_insert_node_to_list_head( APOLLO_LIST_HEAD ** list, APOLLO_LIST_HEAD * node );
INT32_T count_apollo_list( APOLLO_LIST_HEAD * list );

VOID  apollo_delete_node_from_list( APOLLO_LIST_HEAD ** list, APOLLO_LIST_HEAD * node, APOLLO_LIST_FILTER_CALLBACK filter, APOLLO_DELETE_NODE_CALLBACK delnode );

#define APOLLO_LIST_ENTRY( pos, list ) for( pos=list; pos; pos=pos->next ) 

typedef RTSTATUS ( * APOLLO_LIST_DO_CALLBACK )( PVOID info );
typedef RTSTATUS ( * APOLLO_LIST_MASK_CALLBACK )( PVOID info );
RTSTATUS apollo_list_each_entry( APOLLO_LIST_HEAD * list, APOLLO_LIST_DO_CALLBACK dofunc );
RTSTATUS apollo_deal_each_entry( APOLLO_LIST_HEAD ** list, APOLLO_LIST_MASK_CALLBACK maskfunc, APOLLO_DELETE_NODE_CALLBACK delnode, APOLLO_LIST_DO_CALLBACK dofunc );
    


#endif//__APOLLO_LIST_H

