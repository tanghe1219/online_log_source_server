#ifndef __APOLLO_SAFE_OPEATION_H
#define __APOLLO_SAFE_OPEATION_H

#include "apollo_systype.h"

VOID     safe_free( PVOID ptr );
INT8_T * safe_strdup( INT8_T * ptr );
RTSTATUS safe_strcasecmp( INT8_T * ptr1, INT8_T * ptr2 );
INT32_T  safe_strcmp( INT8_T * ptr1, INT8_T * ptr2 );
INT32_T  safe_my_strcmp( INT8_T *ptr1, INT8_T  *ptr2 );
INT32_T  safe_memcmp( PVOID ptr1, PVOID ptr2, INT32_T length );
INT32_T  safe_memcpy( PVOID ptr1, PVOID ptr2, INT32_T length );
INT32_T  safe_strlen( INT8_T * ptr );
INT8_T * safe_strstr( INT8_T * ptr1, INT8_T * ptr2 );
INT32_T  safe_ipcmp( INT8_T * ipfmt1, INT8_T * ipfmt2 );
RTSTATUS extra_mkdir( INT8_T * dir );

VOID safe_skip_nonchar_and_nonnum_from_string( INT8_T * string );
INT8_T * safe_strip_special_characters_from_string_head_and_tail( INT8_T * string, INT8_T * characters, INT32_T nchar );

INT8_T * safe_upper_ptr( INT8_T * ptr );
INT8_T * safe_lower_ptr( INT8_T * ptr );

#endif//__APOLLO_SAFE_OPEATION_H

