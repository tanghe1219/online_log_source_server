#ifndef JSON_DATABASE_H_INCLUDE
#define JSON_DATABASE_H_INCLUDE

#include "FileDatabase.h"

typedef struct
{
   char *topdir;
   char *filepath;//前端请求日志路径
   char *appname;//前端请求的应用日志名称
   char *type; //0静态日志 1动态日志
   char *seg_start;//获取日志起始行
   char *seg_end; //获取日志结束行
}JSONREQUEST_INFO;

//解析前端json请求数据
JSONREQUEST_INFO *parseRequstData(const char *body);

//封装放回给前端json数据
char *packageResponseData(FILERESPONSE_INFO_LIST *infos);

//应用日志出错处理
char *responseErrorMsg(const char *type,const char *msg);

char *queryAppInfos(FILERESPONSE_INFO_LIST *infos);

char *responseCodeMsg(int code, const char *msg);

void cleanupRequest(JSONREQUEST_INFO* req);

#endif
