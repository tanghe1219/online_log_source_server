#ifndef __APOLLO_SYSTYPE_H
#define __APOLLO_SYSTYPE_H

#ifndef INT8_T
typedef char                       INT8_T;
#endif

#ifndef UINT8_T
typedef unsigned char              UINT8_T;
#endif

#ifndef INT16_T
typedef short                      INT16_T;
#endif

#ifndef UINT16_T
typedef unsigned short             UINT16_T;
#endif

#ifndef INT32_T
typedef int                        INT32_T;
#endif

#ifndef UINT32_T
typedef unsigned int               UINT32_T;
#endif

#ifndef INT64_T
typedef long long int              INT64_T;
#endif

#ifndef UINT64_T
typedef unsigned long long int     UINT64_T;
#endif

#ifndef VOID
typedef void                       VOID;
#endif

#ifndef PVOID
typedef void *                     PVOID;
#endif

#ifndef RTSTATUS
typedef int                        RTSTATUS;
#endif

#ifndef FLOAT
typedef float                      FLOAT;
#endif

#ifndef DOUBLE
typedef double                     DOUBLE;
#endif

#ifndef BOOL
typedef int                        BOOL;
#endif

#ifndef TRUE
#define TRUE 1
#endif

#ifndef FALSE
#define FALSE 0
#endif

#ifndef LOCALHOST
#define LOCALHOST "127.0.0.1"
#endif

/*******************************************
 *得到指定地址上的一个字节或整数
 *******************************************/
#define  MEM_BYTE(x)  (*((UINT8_T *)(x)))
#define  MEM_INT(x)  (*((UINT32_T *)(x)))
/*******************************************
 *求最大值和最小值
 *******************************************/
#define  MAX(x,y) (((x)>(y))?(x):(y))
#define  MIN(x,y) (((x)<(y))?(x):(y))
/*******************************************
 *将一个字母转换为大写
 *******************************************/
#define  UPCASE(c) (((c)>='a'&&(c)<='z')?((c)-0x20):(c))
/*******************************************
 *将一个字母转换为小写
 *******************************************/
#define  LOWCASE(c) (((c)>='A'&&(c)<='Z')?((c)+0x20):(c))
/*******************************************
 *判断字符是不是10进值的数字
 *******************************************/
#define  DECCHK(c) ((c)>='0'&&(c)<='9')

/*******************************************
 *得到一个member在结构体(struct)中的偏移量
 *******************************************/
#define OFFSET_OF_MEMBER(type,member) ((UINT32_T)&(((type*)0)->mebber))
/*******************************************
 *得到一个结构体中member所占用的字节数
 *******************************************/
#define SIZEOF_MEMBER(type,member) sizeof(((type*)0)->member)
/*******************************************
 *得到一个结构体中member的类型
 *******************************************/
#define TYPEOF_MEMBER(type,member) typeof(((type*)0)->member)
/*******************************************
 *得到一个结构体变量中member的地址
 *******************************************/
#define CONTAINER_OF(ptr,type,member) ({             \
const typeof(((type*)0)->member) * __mptr=(ptr);     \
(type*)((INT8_T*)__mptr-OFFSET_OF_MEMBER(type,member));})

/*******************************************
 *返回数组元素的个数
 *******************************************/
#define ARR_SIZE(a) (sizeof((a))/sizeof((a[0])))

/*******************************************
 *分配内存
 *******************************************/
#define MALLOC(type) (((type*)calloc(sizeof(type),1)))
#define MALLOC_ARRAY(n,type) (((type*)calloc((n)*sizeof(type),1)))


#define MAX_COMMON_FILEPATH_LENGTH 1024
#define MAX_COMMON_BUFFER_LENGTH   1024
#define MAX_COMMON_CMDLINE_LENGTH  1024 
#define MAX_COMMON_READLINE_LENGTH 1024

#define indent_printf(indent,format,args...) do{INT32_T i;for(i=0;i<indent;i++){printf(" ");}printf(format,##args);}while(0);

#endif//__APOLLO_SYSTYPE_H

