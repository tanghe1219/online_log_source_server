#ifndef _RES_REPORTER_H_
#define _RES_REPORTER_H_

#include <string>
#include <vector>

/*
   将系统资源汇报到前端模块
   系统资源: CPU, 内存, 磁盘, 网络
*/

namespace logger {

class ResReporter {

public:
    ResReporter();
    ~ResReporter();

    bool getResource(std::string& content);
};

};

#endif // _RES_REPORTER_H_