#ifndef _INTERFACE_H_
#define _INTERFACE_H_

namespace logger {

//// URL
static const std::string& SYSTEM_RES_URL = "/resource";
static const std::string& SYSTEM_LOG_URL = "/system_log";
static const std::string& QUERY_SYS_LOG_URL = "/system_log/query";
static const std::string& ADD_SYS_LOG_URL = "/system_log/add";
static const std::string& REMOVE_SYS_LOG_URL = "/system_log/remove";
static const std::string& UPDATE_SYS_LOG_URL = "/system_log/update";

static const std::string& APP_LOG_URL = "/app_log";
static const std::string& QUERY_APP_LOG_URL = "/app_log/query";
static const std::string& ADD_APP_LOG_URL = "/app_log/add";
static const std::string& REMOVE_APP_LOG_URL = "/app_log/remove";
static const std::string& UPDATE_APP_LOG_URL = "/app_log/update";
static const std::string& RT_LOG_URL = "/realtime_log";



//// 资源
// response
/*
	{
		"data" : {
			"cpu": "",
			"mem": "",
			"disk": "",
			"recv": "",
			"send": "",
			"topcpu": "",
			"topmem": "",
		}
	}
*/
static const std::string& CPU_ITEM = "cpu";
static const std::string& MEM_ITEM = "mem";
static const std::string& DISK_ITEM = "disk";
static const std::string& NET_RECV_ITEM = "recv";
static const std::string& NET_SEND_ITEM = "send";
static const std::string& CPU_TOP_ITEM = "topcpu";
static const std::string& MEM_TOP_ITEM = "topmem";

///// 日志
// request
/*
    {
		"type": "0", // 0 - system系统日志, 1 - application应用日志
		"file-name": "syslog",
		"file-seg-start": "1", // class = 1有效，需要加载的文件部分分节, 分节开始
		"file-seg-end": "1024", // 分节结束
	}
*/
static const std::string& TYPE_ITEM = "type";
static const std::string& FILE_NAME_ITEM = "file_name";
static const std::string& FULL_FILE_PATH_ITEM = "file_path";
static const std::string& FILE_SEG_START_ITEM = "file_seg_start";
static const std::string& FILE_SEG_END_ITEM = "file_seg_end";

// response
/*
    {
		"type": "0", // 0 - system系统日志, 1 - application应用日志
		"file-name": "syslog",
		"file-path": "/var/log/syslog",
		"class": "0", // 0 - 静态日志, 1 - 动态日志(需要实时刷新)
		"file-seg-start": "1", // class = 1有效，需要加载的文件部分分节, 实际分节开始
		"file-seg-end": "1024", // 实际分节结束
		"data": "",
	}
*/
static const std::string& FILE_SIZE_ITEM = "file_size";
static const std::string& CLASS_ITEM = "class";
static const std::string& DATA_ITEM = "data";
static const std::string& IS_DEFAULT_ITEM = "is_default";

static const std::string& ERROR_ITEM = "error";
static const std::string& ERRMSG_ITEM = "msg";

};

#endif // !_INTERFACE_H_