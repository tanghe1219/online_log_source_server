#include "json/json.h"
#include "application_log.h"
#include "struct.h"
#include "const.h"
#include "interface.h"
#include "./tools/file_operator.h"

namespace logger {

ApplicationLog::ApplicationLog() {
    loadDefaultFileInfo();
}

ApplicationLog::~ApplicationLog() {

}

bool ApplicationLog::loadDefaultFileInfo() {
    // 1. 从缓存中加载
    // 2. 从默认中加载
    std::map<std::string, std::string>::iterator it = DEFAULT_APP_LOG_PATH.begin();

    NetFileInfo file_info;
    for (; it != DEFAULT_APP_LOG_PATH.end(); it++) {
        file_info.reset();
        file_info.file_name = it->first;
        file_info.path_file_name = it->second;
        file_info.is_dynamic = true;
        curr_file_info_.insert(std::make_pair(file_info.file_name, file_info));
    }

    return true;
}

bool ApplicationLog::getParamFileInfo(const std::string& json_param, std::string& content) {

    Json::CharReaderBuilder reader_build;
    Json::CharReader *reader = reader_build.newCharReader();
    Json::Value root;
    Json::String errs;
    Json::StreamWriterBuilder write_builder;
    Json::StreamWriter *writer = write_builder.newStreamWriter();
    Json::Value root_out;
    Json::OStringStream out_stream;
    int errcode;
    std::string errmsg;
    std::string file_content;
    char s_data[16] = {0};

    do {

        if (!reader->parse(json_param.c_str(), json_param.c_str() + json_param.size(), &root, &errs)) {
            errcode = SERVER_INTER_ERR;
            errmsg = errs;
            break;
        }

        Json::Value file_name_val = root.get(FILE_NAME_ITEM.c_str(), "");
        
        if (file_name_val.asString().empty()) {
            errcode = NEED_MUST_PARAM_ERR;
            errmsg = NEED_MUST_PARAM_ERR_STR;
            break;
        }

        Json::Value full_file_path = root.get(FULL_FILE_PATH_ITEM.c_str(), "");

        std::map<std::string, NetFileInfo>::iterator iter_file_info = curr_file_info_.find(file_name_val.asString());
        if (!full_file_path.asString().empty() && iter_file_info == curr_file_info_.end()) {
            // 新建的文件查看
            NetFileInfo net_file_tmp;
            net_file_tmp.file_name = file_name_val.asString();
            net_file_tmp.path_file_name = full_file_path.asString();
            curr_file_info_.insert(std::make_pair(net_file_tmp.file_name, net_file_tmp));
        } else if (iter_file_info == curr_file_info_.end()) {
            // 必须保证文件存在，前台会进行两次发送, 1. 自定义的文件设置, 2. 选中展示某个日志文件
            errcode = UNKNOW_FILE_ERR;
            errmsg = UNKNOW_FILE_ERR_STR;
            break;
        }

        NetFileInfo &net_file_info = curr_file_info_[file_name_val.asString()];

        Json::Value type_val = root.get(TYPE_ITEM.c_str(), "");
        if (type_val.asString().empty()) {
            // check it
        }

        // 判断是否为动态文件
        size_t old_size = net_file_info.file_size;
        size_t new_size = FileOperator::getFileSize(net_file_info.path_file_name);
        if (old_size != new_size) {
            net_file_info.is_dynamic = true;
        }

        Json::Value file_seg_start = root.get(FILE_SEG_START_ITEM.c_str(), "");
        ssize_t new_seg_start = atoi(file_seg_start.asString().c_str());

        Json::Value file_seg_end = root.get(FILE_SEG_END_ITEM.c_str(), "");
        ssize_t new_seg_end = atoi(file_seg_end.asString().c_str());

        if (new_seg_start < -1) {
            new_seg_start = 0;
        }

        // TODO: fixed me, add big file detect
        if (new_seg_end <= 0) {
            new_seg_end = new_size - new_seg_start;
        }

        root_out[TYPE_ITEM.c_str()] = "0" ; // 0 - system系统日志, 1 - application应用日志
        root_out[FILE_NAME_ITEM.c_str()] = net_file_info.file_name;
        root_out[FULL_FILE_PATH_ITEM.c_str()] = net_file_info.path_file_name;
        root_out[CLASS_ITEM.c_str()] = net_file_info.is_dynamic ? "1" : "0";

        net_file_info.seg_start = new_seg_start;
        net_file_info.seg_end = new_seg_end;

        FileOperator file_oper;
        if (!file_oper.open(net_file_info.path_file_name)) {
            errcode = SERVER_INTER_ERR;
            errmsg = SERVER_INTER_ERR_STR;
            break;
        }

        if (!file_oper.read(net_file_info.seg_start, net_file_info.seg_end, file_content)) {
            errcode = SERVER_INTER_ERR;
            errmsg = SERVER_INTER_ERR_STR;
            break;
        }

        memset(s_data, 0x0, sizeof(s_data));
        sprintf(s_data, "%u", net_file_info.seg_start);
        root_out[FILE_SEG_START_ITEM.c_str()] =  s_data;
        memset(s_data, 0x0, sizeof(s_data));
        sprintf(s_data, "%u", net_file_info.seg_end);
        root_out[FILE_SEG_END_ITEM.c_str()] = s_data;
        root_out[DATA_ITEM.c_str()] = file_content;

        if (writer->write(root_out, &out_stream) == 0) {
            content = out_stream.str();
            return true;
        }
    } while (0);
    
    memset(s_data, 0x0, sizeof(s_data));
    sprintf(s_data, "%d", errcode);
    root_out[ERROR_ITEM.c_str()] = s_data;
    root_out[ERRMSG_ITEM.c_str()] = errmsg;

    writer->write(root_out, &out_stream);
    content = out_stream.str();
    return false;
}

bool ApplicationLog::addLogFile(const std::string& json_param, std::string& content) {

    Json::CharReaderBuilder reader_build;
    Json::CharReader *reader = reader_build.newCharReader();
    Json::Value root;
    Json::String errs;
    Json::StreamWriterBuilder write_builder;
    Json::StreamWriter *writer = write_builder.newStreamWriter();
    Json::Value root_out;
    Json::OStringStream out_stream;
    int errcode;
    std::string errmsg;
    std::string file_content;
    char s_data[16] = {0};

    do {

        if (!reader->parse(json_param.c_str(), json_param.c_str() + json_param.size(), &root, &errs)) {
            errcode = SERVER_INTER_ERR;
            errmsg = errs;
            break;
        }

        Json::Value file_name_val = root.get(FILE_NAME_ITEM.c_str(), "");
        if (file_name_val.empty()) {
            errcode = NEED_MUST_PARAM_ERR;
            errmsg = NEED_MUST_PARAM_ERR_STR;
            break;
        }
        
        // 必须保证文件存在，前台会进行两次发送, 1. 自定义的文件设置, 2. 选中展示某个日志文件
        std::map<std::string, NetFileInfo>::iterator iter_file_info = 
            curr_file_info_.find(file_name_val.asString());
        if (iter_file_info != curr_file_info_.end()) {
            errcode = FILE_EXIST_ERR;
            errmsg = FILE_EXIST_ERR_STR;
            break;
        }

        std::string path_content;
        if (FileOperator::getFileName("/var/log", file_name_val.asString(), path_content)) {
            memset(s_data, 0x0, sizeof(s_data));
            sprintf(s_data, "%d", 0);
            root_out[ERROR_ITEM.c_str()] = s_data;
            root_out[DATA_ITEM.c_str()] = path_content;
            writer->write(root_out, &out_stream);
            content = out_stream.str();
            return true;
        }

        // 没有找到，从"/"开始查找
        if (FileOperator::getFileName("/", file_name_val.asString(), path_content)) {
            memset(s_data, 0x0, sizeof(s_data));
            sprintf(s_data, "%d", 0);
            root_out[ERROR_ITEM.c_str()] = s_data;
            root_out[DATA_ITEM.c_str()] = path_content;
            writer->write(root_out, &out_stream);
            content = out_stream.str();
            return true;
        }

    } while (0);

    errcode = UNKNOW_FILE_ERR;
    errmsg = UNKNOW_FILE_ERR_STR;

    memset(s_data, 0x0, sizeof(s_data));
    sprintf(s_data, "%d", errcode);
    root_out[ERROR_ITEM.c_str()] = s_data;
    root_out[ERRMSG_ITEM.c_str()] = errmsg;
    writer->write(root_out, &out_stream);
    content = out_stream.str();
    return false;
}

const std::string& ApplicationLog::getFileContent() {

}

};

