#include "use_mem.h"
#include "../tools/string_operator.h"

namespace logger {

	FileOperator UseMem::file_oper;

	void UseMem::getMemUsed(int& usage) {
		bool ret_flag = true;
		if (!file_oper.isOpen()) {
			ret_flag = file_oper.open(MEM_PROC);

			if (!ret_flag) {
				return;
			}
		}

		std::vector<std::string> contents;
		file_oper.getLinesContent(5, contents);

		long int total_free_mem = 0;

		// free memory
		for (size_t i = 1; i < contents.size(); ++i) {

			// avail mem ���ܼ���
			if (i == 2) {
				continue;
			}
			
			size_t cur_pos = contents[i].find(":");
			if (cur_pos == std::string::npos) {
				continue;
			}

			std::string value = contents[i].substr(cur_pos + 1);
			if (value.empty()) {
				continue;
			}

			long int curr_free_mem = 0;
			std::vector<std::string> vec = StringOperator::split(value, ' ');
			if (vec.size() == 2) {
				curr_free_mem = atol(vec[0].c_str());
			}

			total_free_mem += curr_free_mem;
		}

		// total memory
		size_t cur_pos = contents[0].find(":");
		if (cur_pos == std::string::npos) {
			file_oper.rewind();
			return;
		}

		std::string value = contents[0].substr(cur_pos + 1);
		if (value.empty()) {
			file_oper.rewind();
			return;
		}

		long int total_mem = 0;
		std::vector<std::string> vec = StringOperator::split(value, ' ');
		if (vec.size() == 2) {
			total_mem = atol(vec[0].c_str());
		}
		
		usage = (int)(double)(total_mem - total_free_mem) * 100 / total_mem ;

		file_oper.rewind();
	}

};
