#ifndef _USE_NET_H_
#define _USE_NET_H_

#include "../tools/file_operator.h"

namespace logger {

	static const std::string& NET_DEV = "/proc/net/dev";

	class UseNet {

	public:
		static void getNetUsed(long int& recv_rate, long int& trans_rate);

	private:
		static FileOperator file_oper;
		static long int last_recv;
		static long int last_trans;
		static long int last_rec_nsec;
	};

};

#endif // _USE_NET_H_