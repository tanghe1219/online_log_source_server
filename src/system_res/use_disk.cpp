#include "use_disk.h"
#include <time.h>
#include <sys/vfs.h>
#include "../tools/string_operator.h"

namespace logger {

	FileOperator UseDisk::file_oper;
	std::vector<std::string> UseDisk::mount_point_sets;
	time_t UseDisk::last_timestamp_;

	bool UseDisk::getMountPoints() {
		time_t curr_timestamp;
		time(&curr_timestamp);

		if (curr_timestamp - last_timestamp_ < QUERY_DATA_SPAN_DISK) {
			return true;
		}

		last_timestamp_ = curr_timestamp;

		bool ret_flag = true;
		if (!file_oper.isOpen()) {
			ret_flag = file_oper.open(MOUNT_POINTS);

			if (!ret_flag) {
				return false;
			}
		}

		size_t total_lines = 0;
		file_oper.getTotalLines(total_lines);

		std::vector<std::string> contents;
		file_oper.getLinesContent(total_lines, contents);

		for (size_t i = 0; i < contents.size(); ++i) {
			std::vector<std::string> vec = StringOperator::split(contents[i], ' ');
			if (vec.size() < 2) {
				continue;
			}

			std::string fs_item = vec[1];
			std::size_t found = fs_item.find("/");
			if (found == std::string::npos) {
				continue;
			}

			std::string fs_string;
			found = fs_item.find("/", found + 1);
			if (found == std::string::npos) {
				fs_string = fs_item;
			}
			else {
				fs_string = fs_item.substr(0, found);
			}

			bool is_exists = false;
			std::vector<std::string>::iterator it = mount_point_sets.begin();
			for (; it != mount_point_sets.end(); it++) {
				if (*it == fs_string) {
					is_exists = true;
					break;
				}
			}

			if (!is_exists) {
				mount_point_sets.push_back(fs_string);
			}
		}

		file_oper.rewind();
		return true;
	}

	bool UseDisk::getTotalAndUsedDisk(const std::string& path, long int& total, long int& used) {
		struct statfs disk_info;
		if (statfs(path.c_str(), &disk_info) == -1) {
			return false;
		}

		// long block_size = disk_info.f_bsize; //每个block里包含的字节数 
		long total_size = disk_info.f_blocks * 1; //总的字节数，f_blocks为block的数目 
		long free_disk = disk_info.f_bfree * 1; //剩余空间的大小 
	
		total = total_size;
		used = total - free_disk;

		return true;
	}

	void UseDisk::getDiskUsed(int& used_rate) {

		if (!getMountPoints()) {
			return;
		}

		long int total_all = 0, used_all = 0;
		long int total = 0, used = 0;
		std::vector<std::string>::iterator it = mount_point_sets.begin();
		for (; it != mount_point_sets.end(); ) {
			total = used = 0;
			if (!getTotalAndUsedDisk(*it, total, used)) {
				it = mount_point_sets.erase(it);
				continue;
			} 

			total_all += total;
			used_all += used;

			it++;
		}

		used_rate = (int)(double)(used_all * 100) / total_all;
	}

};