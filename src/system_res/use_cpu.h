#ifndef _USE_CPU_H_
#define _USE_CPU_H_

#include <string>
#include <map>
#include "../tools/file_operator.h"

namespace logger {

	const static std::string& PROC_CPU_ENTRY = "/proc/stat";
	const static std::string& PROC_PROCESS_ENTRY = "/proc/%d/stat";
	static const int QUERY_DATA_SPAN_CPU = 1;

	struct ProcessInfo {
		std::string process_name;
		long int cpu_value;
		long int mem_value;
	};

	typedef std::map<long int, ProcessInfo> TopValueMap;
	typedef std::multimap<long int, std::string> TopValueMulMap;

	class UseCpu {

	public:
		static void getCpuUsed(double& cpu_used);
		static void getTopCpuAndMem(TopValueMulMap& cpu_val, TopValueMulMap& mem_val);

	private:
		static FileOperator cpu_file_oper;
		static FileOperator top_file_oper;
		static TopValueMap last_top_used;
		static TopValueMulMap last_cpu_val;
		static TopValueMulMap last_mem_val;
		static long int last_cpu_total_time;
		static long int last_cpu_idle_time;
		static double last_cpu_used;
		static time_t last_cpu_timestamp_;
		static time_t last_top_timestamp_;
	};

};

#endif
