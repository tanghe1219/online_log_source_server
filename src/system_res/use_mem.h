#ifndef _USE_MEM_H_
#define _USE_MEM_H_

#include <string>
#include "../tools/file_operator.h"

namespace logger {

	static const std::string& MEM_PROC = "/proc/meminfo";

	class UseMem {

	public:
		static void getMemUsed(int& usage);

	private:
		static FileOperator file_oper;
	};

};

#endif
