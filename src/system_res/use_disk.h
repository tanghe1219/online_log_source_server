#ifndef _USE_DISK_H_
#define _USE_DISK_H_

#include <vector>
#include <string>
#include "../tools/file_operator.h"

namespace logger {

	static const std::string& MOUNT_POINTS = "/proc/mounts";
	static const int QUERY_DATA_SPAN_DISK = 5;

	class UseDisk {
	public:
		static bool getMountPoints();
		static bool getTotalAndUsedDisk(const std::string& path, long int& total, long int& used);
		static void getDiskUsed(int& used);

	private:
		static FileOperator file_oper;
		static std::vector<std::string> mount_point_sets;
		static time_t last_timestamp_;
	};

}

#endif
