#include "use_cpu.h"
#include <sys/types.h>
#include <dirent.h>
#include <cassert>
#include "../tools/string_operator.h"

namespace logger {

	FileOperator UseCpu::cpu_file_oper;
	FileOperator UseCpu::top_file_oper;
	TopValueMap UseCpu::last_top_used;
	TopValueMulMap UseCpu::last_cpu_val;
	TopValueMulMap UseCpu::last_mem_val;
	time_t UseCpu::last_cpu_timestamp_;
	time_t UseCpu::last_top_timestamp_;
	long int UseCpu::last_cpu_total_time;
	long int UseCpu::last_cpu_idle_time;
	double UseCpu::last_cpu_used;

	void UseCpu::getCpuUsed(double& cpu_used) {
		time_t curr_timestamp;
		time(&curr_timestamp);

		// 1s一次
		if (curr_timestamp - last_cpu_timestamp_ < QUERY_DATA_SPAN_CPU) {
			cpu_used = last_cpu_used;
			return;
		}
		
		bool ret_flag = true;
		if (!cpu_file_oper.isOpen()) {
			ret_flag = cpu_file_oper.open(PROC_CPU_ENTRY);

			if (!ret_flag) {
				return;
			}
		}

		std::vector<std::string> contents;
		cpu_file_oper.getLinesContent(1, contents);
		if (contents.size() != 1) {
			return;
		}

		std::vector<std::string> vec = StringOperator::split(contents[0], ' ');
		if (vec.size() != 11) {
			return;
		}

		// CPU指标：user，nice, system, idle, iowait, irq, softirq
		// cpu  65508 1996 77727 53461048 62432 83355 37108 0 0 0
		long int total_time = atoi(vec[1].c_str()) + atoi(vec[2].c_str()) + atoi(vec[3].c_str()) + \
			atoi(vec[4].c_str()) + atoi(vec[5].c_str()) + atoi(vec[6].c_str()) + atoi(vec[7].c_str());
		long int idle_time = atoi(vec[4].c_str());
		if (last_cpu_total_time == 0 && idle_time == 0) {
			last_cpu_total_time = total_time;
			last_cpu_idle_time = idle_time;
			cpu_used = 0;
			return;
		}

		cpu_used = 100 * (1 - (idle_time - last_cpu_idle_time) * 1.0 / (total_time - last_cpu_total_time));
		last_cpu_total_time = total_time;
		last_cpu_idle_time = idle_time;
		last_cpu_used = cpu_used;

		cpu_file_oper.rewind();
	}


	void UseCpu::getTopCpuAndMem(TopValueMulMap& cpu_val, TopValueMulMap& mem_val) {
		DIR* proc_dir;
		struct dirent* pid_dir;
		pid_t pid;

		time_t curr_timestamp;
		time(&curr_timestamp);

		if (curr_timestamp - last_top_timestamp_ < QUERY_DATA_SPAN_CPU) {
			if (last_top_used.size() == 0) {
				return;
			}

			cpu_val = last_cpu_val;
			mem_val = last_mem_val;
			return;
		}

		TopValueMap now_top_used;

		last_top_timestamp_ = curr_timestamp;

		proc_dir = opendir("/proc");
		if (!proc_dir) {
			return;
		}

		char filename[64];
		while (pid_dir = readdir(proc_dir)) {
			if (!isdigit(pid_dir->d_name[0])) {
				continue;
			}

			pid = atoi(pid_dir->d_name);
			sprintf(filename, PROC_PROCESS_ENTRY.c_str(), pid);

			bool ret_flag = true;
			if (!top_file_oper.isOpen()) {
				ret_flag = top_file_oper.open(filename);

				if (!ret_flag) {
					return;
				}
			}

			std::vector<std::string> contents;
			top_file_oper.getLinesContent(1, contents);
			if (contents.size() != 1) {
				continue;
			}

			std::vector<std::string> vec = StringOperator::split(contents[0], ' ');
			if (vec.size() < 25) {
				return;
			}

			/* 1. pid： 进程ID.
			2. comm: task_struct结构体的进程名
			3. state : 进程状态, 此处为S
			4. ppid : 父进程ID （父进程是指通过fork方式，通过clone并非父进程）
			5. pgrp：进程组ID
			6. session：进程会话组ID
			7. tty_nr：当前进程的tty终点设备号
			8. tpgid：控制进程终端的前台进程号
			9. flags：进程标识位，定义在include / linux / sched.h中的PF_*, 此处等于1077952832
			10. minflt： 次要缺页中断的次数，即无需从磁盘加载内存页.比如COW和匿名页
			11. cminflt：当前进程等待子进程的minflt
			12. majflt：主要缺页中断的次数，需要从磁盘加载内存页.比如map文件
			13. majflt：当前进程等待子进程的majflt
			14. utime : 该进程处于用户态的时间，单位jiffies，此处等于166114
			15. stime : 该进程处于内核态的时间，单位jiffies，此处等于129684
			16. cutime：当前进程等待子进程的utime
			17. cstime : 当前进程等待子进程的utime
			18. priority : 进程优先级, 此次等于10.
			19. nice : nice值，取值范围[19, -20]，此处等于 - 10
			20. num_threads : 线程个数, 此处等于221
			21. itrealvalue : 该字段已废弃，恒等于0
			22. starttime：自系统启动后的进程创建时间，单位jiffies，此处等于2284
			23. vsize：进程的虚拟内存大小，单位为bytes
			24. rss : 进程独占内存 + 共享库，单位pages，此处等于93087
			25. rsslim : rss大小上限
			*/
			long int cpu_time = atol(vec[13].c_str()) + atol(vec[14].c_str()) + atol(vec[15].c_str()) \
				+ atol(vec[16].c_str());
			long int mem_val = atol(vec[23].c_str());

			long int pid = atol(vec[0].c_str());
			TopValueMap::iterator it_top = now_top_used.find(pid);
			if (it_top != now_top_used.end() && it_top->second.process_name == vec[1]) {
				it_top->second.cpu_value += cpu_time;
				it_top->second.mem_value = mem_val;
			} else {
				ProcessInfo proc_info;
				proc_info.cpu_value = cpu_time;
				proc_info.mem_value = mem_val;
				proc_info.process_name = vec[1];
				now_top_used.insert(std::make_pair(pid, proc_info));
			}
			
			top_file_oper.close();
		}

		closedir(proc_dir);

		// 更新结果
		if (last_top_used.size() == 0) {
			last_top_used.swap(now_top_used);
			return;
		}
		
		TopValueMap::iterator it_curr_top = now_top_used.begin();
		for (; it_curr_top != now_top_used.end(); it_curr_top++) {
			TopValueMap::iterator it_last_top = last_top_used.find(it_curr_top->first);
			if (it_last_top == last_top_used.end()) {
				continue;
			}

			if (it_last_top->second.process_name != it_curr_top->second.process_name) {
				continue;
			}

			cpu_val.insert(std::make_pair(it_curr_top->second.cpu_value - it_last_top->second.cpu_value, 
				it_curr_top->second.process_name));
			mem_val.insert(std::make_pair(it_curr_top->second.mem_value, 
				it_curr_top->second.process_name));

			last_cpu_val.clear();
			last_cpu_val = cpu_val;
			last_mem_val.clear();
			last_mem_val = mem_val;
		}

		last_top_used.swap(now_top_used);
	}

};