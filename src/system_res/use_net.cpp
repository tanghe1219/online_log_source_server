#include "use_net.h"
#include <vector>
#include <string>
#include <string.h>
#include <sys/time.h> 
#include "../tools/file_operator.h"
#include "../tools/string_operator.h"

namespace logger {

	FileOperator UseNet::file_oper;
	long int UseNet::last_recv;
	long int UseNet::last_trans;
	long int UseNet::last_rec_nsec;

	void UseNet::getNetUsed(long int& recv_rate, long int& trans_rate) {
		bool ret_flag = true;
		if (!file_oper.isOpen()) {
			ret_flag = file_oper.open(NET_DEV);

			if (!ret_flag) {
				return;
			}
		}

		struct timeval curr_tv;
		gettimeofday(&curr_tv, NULL);

		size_t total_lines = 0;
		file_oper.getTotalLines(total_lines);

		std::vector<std::string> contents;
		file_oper.getLinesContent(total_lines, contents);

		long int total_recv = 0, total_trans = 0;
		for (size_t i = 0; i < contents.size(); ++i) {
			if (strncmp(contents[i].c_str(), "Inter-", strlen("Inter-")) == 0) {
				continue;
			}

			if (strncmp(contents[i].c_str(), " face", strlen(" face")) == 0) {
				continue;
			}

			if (strncmp(contents[i].c_str(), "    lo:", strlen("    lo:")) == 0) {
				continue;
			}

			size_t cur_pos = contents[i].find(":");
			if (cur_pos == std::string::npos) {
				continue;
			}

			std::string value = contents[i].substr(cur_pos + 2);
			if (value.empty()) {
				continue;
			}

			long int cur_recv = 0, cur_trans = 0;
			std::vector<std::string> vec = StringOperator::split(value, ' ');
			if (vec.size() == 16) {
				cur_recv = atol(vec[0].c_str());
				cur_trans = atol(vec[8].c_str());
			}

			total_recv += cur_recv;
			total_trans += cur_trans;
		}

		long int curr_rec_nsec = curr_tv.tv_sec * 1000 + curr_tv.tv_usec / 1000;
		if (last_recv > 0) {
			double d_recv_rate = (double)(total_recv - last_recv) * 1000 / (curr_rec_nsec - last_rec_nsec);
			recv_rate = (int)d_recv_rate ; // b/s
		}

		if (last_trans > 0) {
			double d_trans_rate = (double)(total_trans - last_trans) * 1000 / (curr_rec_nsec - last_rec_nsec);
			trans_rate = (int)d_trans_rate ; // b/s
		}

		last_rec_nsec = curr_rec_nsec;
		last_recv = total_recv;
		last_trans = total_trans;

		file_oper.rewind();
	}

};