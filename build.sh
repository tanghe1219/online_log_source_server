#!/bin/sh

set -x

PROJECT_DIR=`pwd`/
SOURCE_DIR=`pwd`/src
BUILD_DIR=${BUILD_DIR:-./build}
#BUILD_TYPE=${BUILD_TYPE:-release}
BUILD_TYPE=${BUILD_TYPE:-Debug}
BUILD_ARCH=`uname -p`
#INSTALL_DIR=${INSTALL_DIR:-../${BUILD_TYPE}-install-cpp11}
#           -DCMAKE_INSTALL_PREFIX=$INSTALL_DIR \
CXX=${CXX:-g++}

mkdir -p $BUILD_DIR/ \
  && cd $BUILD_DIR/ \
  && cmake \
           -DCMAKE_BUILD_TYPE=$BUILD_TYPE \
           -DPROJECT_DIR=$PROJECT_DIR \
           -DBUILD_ARCH=$BUILD_ARCH \
           $SOURCE_DIR \
  && make $*

mkdir -p ${PROJECT_DIR}/bin/ 2&> /dev/null
\cp -rf ${PROJECT_DIR}/build/bin/* ${PROJECT_DIR}/bin/

# now use static library
# library
# ln -s ${PROJECT_DIR}/lib/${BUILD_ARCH}/libjsoncpp.so.1.9.3 /usr/lib64/libjsoncpp.so.24 2&> /dev/null
# ln -s /usr/lib64/libjsoncpp.so.24 /usr/lib64/libjsoncpp.so 2&> /dev/null

# Use the following command to run all the unit tests
# at the dir $BUILD_DIR/$BUILD_TYPE :
# CTEST_OUTPUT_ON_FAILURE=TRUE make test

# cd $SOURCE_DIR && doxygen
